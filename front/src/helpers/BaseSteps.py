from selene.api import *
import allure


class BaseSteps:

    @allure.step("Перейти на страницу")
    def go_to_page(self, url):
        browser.open_url(url)

    @allure.step("Завершить работу браузера")
    def quit_browser(self):
        browser.quit()

    @allure.step("Закрыть браузер")
    def close_browser(self):
        browser.close()

    @allure.step("Закрыть вкладку")
    def close_tab(self):
        browser.close_current_tab()

    @allure.step("Очистить локал")
    def clear_local(self):
        browser.clear_local_storage()

    @allure.step("Очистить сессию")
    def clear_session(self):
        browser.clear_session_storage()

    @allure.step("Добавить ожидание на тест, если имеется лоадер")
    def wait_but_search(self, condition):
        browser.wait_to(condition)

    @allure.step("Скролл вниз")
    def scroll_down(self):
        browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")

    @allure.step("Скролл вверх")
    def scroll_up(self):
        browser.execute_script("window.scrollTo(0, -1000)")

    @allure.step("Переключиться на следующую вкладку")
    def next_tab(self):
        browser.switch_to_next_tab()

    @allure.step("Переключиться на предыдущую вкладку")
    def previous_tab(self):
        browser.switch_to_previous_tab()

    @allure.step("Переключиться на первую вкладку")
    def first_tab(self):
        browser.switch_to_tab(0)

    @allure.step("Ждать пока не появится элемент")
    def wait_until_find(self, condition):
        browser.wait_until(condition)
