class Urlbase:
    Test_Url = 'http://scroll-test.lan/'
    Prod_Url = 'https://scroll.irlix.com/'
    YaUrl = 'http://ya.ru/'


class Endpoints:
    creators = Urlbase.Test_Url + 'creators'
    users = Urlbase.Test_Url + 'users'
    geolocation_employees = Urlbase.Test_Url + 'geolocation?tab=employees'
    geolocation_offices = Urlbase.Test_Url + 'geolocation?tab=offices'
    lk = Urlbase.Test_Url + 'users/viktor.goldov'
    settings_about_gen = Urlbase.Test_Url + 'settings?tab=about'
    settings_notifications = Urlbase.Test_Url + 'settings?tab=notifications'
    settings_personalization = Urlbase.Test_Url + 'settings?tab=personalization'
    start_page = Urlbase.Test_Url + 'feed'
    auf_page = 'http://keycloak.lan/auth/realms/irlix-realm/protocol/openid-connect/auth?client_id' \
               '=irlix-social-front&redirect_uri=http%3A%2F%2Fscroll-test.lan'
