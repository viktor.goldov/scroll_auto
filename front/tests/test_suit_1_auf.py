import allure
import pytest
from front.pages.auf.auf_steps import StepsAuf as Sa
from front.pages.auf.auf_checks import ChecksAuf as Ca
from front.src.helpers.BaseSteps import BaseSteps as Bs
from front.src.data.login import ValidData as Vd, TestData as Td
from front.src.data.Urls import Endpoints as En


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Авторизация')
@allure.testcase('SCR-67 : Успешно авторизоваться, используя валидный логин и пароль.')
@pytest.mark.auf
@pytest.mark.smoke
def test_src_67():
    Bs().go_to_page(En.start_page)
    Sa().input_login(Vd.front_login_user)
    Sa().input_password(Vd.front_password_user)
    Sa().click_enter()
    Ca().check_auf()
    Bs().quit_browser()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Авторизация')
@allure.testcase('SCR-68 : Авторизоваться, используя не валидный логин и валидный пароль.')
@pytest.mark.auf
def test_src_68():
    Bs().go_to_page(En.start_page)
    Sa().input_login(Td.front_login_invalid)
    Sa().input_password(Vd.front_password_user)
    Sa().click_enter()
    Ca().check_auf_bad()
    Bs().quit_browser()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Авторизация')
@allure.testcase('SCR-69 : Авторизоваться, используя валидный логин и не валидный пароль.')
@pytest.mark.auf
def test_src_69():
    Bs().go_to_page(En.start_page)
    Sa().input_login(Vd.front_login_user)
    Sa().input_password(Td.front_password_invalid)
    Sa().click_enter()
    Ca().check_auf_bad()
    Bs().quit_browser()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Авторизация')
@allure.testcase('SCR-70 : Авторизоваться, используя валидный логин и пустое поле Пароль.')
@pytest.mark.auf
def test_src_70():
    Bs().go_to_page(En.start_page)
    Sa().input_login(Vd.front_login_user)
    Sa().input_password(Td.front_password_empty)
    Sa().click_enter()
    Ca().check_auf_bad()
    Bs().quit_browser()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Авторизация')
@allure.testcase('SCR-71 : Авторизоваться, используя валидный пароль и пустое поле Логин')
@pytest.mark.auf
def test_src_71():
    Bs().go_to_page(En.start_page)
    Sa().input_login(Td.front_login_empty)
    Sa().input_password(Vd.front_password_user)
    Sa().click_enter()
    Ca().check_auf_bad()
    Bs().quit_browser()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Авторизация')
@allure.testcase('SCR-72 : Авторизоваться, оставив поля Логин и Пароль пустыми')
@pytest.mark.auf
def test_src_72():
    Bs().go_to_page(En.start_page)
    Sa().input_login(Td.front_login_empty)
    Sa().input_password(Td.front_password_empty)
    Sa().click_enter()
    Ca().check_auf_bad()
    Bs().quit_browser()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Авторизация')
@allure.testcase('SCR-213 : Авторизоваться, введя пробелы перед/после валидного логина.')
@pytest.mark.auf
def test_src_213():
    Bs().go_to_page(En.start_page)
    Sa().input_login(Td.front_login_probel)
    Sa().input_password(Vd.front_password_user)
    Sa().click_enter()
    Ca().check_auf()
    Bs().quit_browser()
