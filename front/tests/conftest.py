import pytest
from selene.api import *

from front.pages.auf.auf_steps import StepsAuf as Sa
from front.pages.auf.auf_checks import ChecksAuf as Ca
from front.src.helpers.BaseSteps import BaseSteps as Bs
from front.src.data.login import ValidData as Vd
from front.src.data.Urls import Endpoints as En
from front.pages.start.start_page import LocatorsStart as Ls
from front.pages.start.start_checks import StartChecks as Sc

browser.config.window_height = 1080
browser.config.window_width = 1920


@pytest.fixture()
def authorization_user_data():
    """Авторизация в соцсети как пользователь до начала теста и переход на главную страницу"""
    Bs().go_to_page(En.start_page)
    Sa().input_login(Vd.front_login_user)
    Sa().input_password(Vd.front_password_user)
    Sa().click_enter()
    Ca().check_auf()

    yield  # здесь происходит тестирование

    """Закрытие браузера по окончанию теста"""
    Bs().quit_browser()


@pytest.fixture()
def authorization_admin_data():
    """Авторизация в соцсети как пользователь до начала теста и переход на главную страницу"""
    Bs().go_to_page(En.start_page)
    Sa().input_login(Vd.front_login_admin)
    Sa().input_password(Vd.front_password_admin)
    Sa().click_enter()
    Ca().check_auf()

    yield  # здесь происходит тестирование

    """Закрытие браузера по окончанию теста"""
    Bs().quit_browser()


@pytest.fixture()
def authorization_admin_data_plus_del_created_post():
    """Авторизация в соцсети как пользователь до начала теста и переход на главную страницу"""
    Bs().go_to_page(En.start_page)
    Sa().input_login(Vd.front_login_admin)
    Sa().input_password(Vd.front_password_admin)
    Sa().click_enter()
    Ca().check_auf()

    yield  # здесь происходит тестирование

    """Удаление поста в конце теста"""
    Bs().wait_until_find(Ls.first_post_block)
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_delete.click()
    Bs().wait_until_find(Ls.post_modal_button_yea_for_del)
    Ls.post_modal_button_yea_for_del.click()
    Sc().check_delete_post(Ls.name_of_post)

    """Закрытие браузера по окончанию теста"""
    Bs().quit_browser()



