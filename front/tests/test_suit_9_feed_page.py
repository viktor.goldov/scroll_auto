import allure
import pytest
from front.pages.start.start_page import LocatorsStart as Ls
from front.pages.start.start_checks import StartChecks as Sc
from front.pages.start.start_steps import StartSteps as Ss
from front.pages.lk.lk_page import LocatorsLk as Llk
from front.pages.admin.admin_steps import AdminSteps as As
from front.pages.auf.auf_steps import StepsAuf as Sa
from front.pages.auf.auf_checks import ChecksAuf as Ca
from front.src.data.login import ValidData as Vd
from front.src.helpers.BaseSteps import BaseSteps as Bs
from selenium.webdriver.common.keys import Keys


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-20 : Закрепить пост с содержимым: текст')
@pytest.mark.feed_page
def test_src_20(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    """Закрепляем пост"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_to_fix.click()
    Ls.post_modal_button_yea_for_fix.click()
    Sc().check_fix_post()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-21 : Открепить пост с содержимым: текст')
@pytest.mark.feed_page
def test_src_21(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    """Закрепляем пост"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_to_fix.click()
    Ls.post_modal_button_yea_for_fix.click()
    Sc().check_fix_post()
    """Открепляем пост"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_to_fix.click()
    Ls.post_modal_button_yea_for_fix.click()
    Sc().check_no_fix_post()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-22 : Закрепить пост с содержимым: текст + картинки ')
@pytest.mark.feed_page
def test_src_22(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.add_picture_button.click()
    Ss().put_picture_post(Llk.url_picture_jpg)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    Sc().check_picture_in_post(Ls.count_pictures_in_first_post)
    """Закрепляем пост"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_to_fix.click()
    Ls.post_modal_button_yea_for_fix.click()
    Sc().check_fix_post()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-23 : Открепить пост с содержимым: текст + картинки ')
@pytest.mark.feed_page
def test_src_23(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.add_picture_button.click()
    Ss().put_picture_post(Llk.url_picture_jpg)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    Sc().check_picture_in_post(Ls.count_pictures_in_first_post)
    """Закрепляем пост"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_to_fix.click()
    Ls.post_modal_button_yea_for_fix.click()
    Sc().check_fix_post()
    """Открепляем пост"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_to_fix.click()
    Ls.post_modal_button_yea_for_fix.click()
    Sc().check_no_fix_post()


@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-24 : Закрепить пост при уже закрепленном посте')
@pytest.mark.feed_page
def test_src_24(authorization_admin_data):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.add_picture_button.click()
    Ss().put_picture_post(Llk.url_picture_jpg)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    Sc().check_picture_in_post(Ls.count_pictures_in_first_post)
    """Закрепляем пост"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_to_fix.click()
    Ls.post_modal_button_yea_for_fix.click()
    Sc().check_fix_post()
    """Создать второй пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post+'NEW')
    Ls.add_picture_button.click()
    Ss().put_picture_post(Llk.url_picture_png)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    Sc().check_picture_in_post(Ls.count_pictures_in_first_post)
    """Закрепляем второй пост"""
    Ls.first_post_block[1].context_click()
    Ls.post_open_modal[1].click()
    Ls.post_modal_to_fix.click()
    Ls.post_modal_button_yea_for_fix.click()
    Sc().check_fix_post()
    """Далее удаляем первый пост"""
    Ls.first_post_block[1].context_click()
    Ls.post_open_modal[1].click()
    Ls.post_modal_delete.click()
    Ls.post_modal_button_yea_for_del.click()
    Sc().check_delete_post(Ls.name_of_post)
    """Далее удаляем второй пост"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_delete.click()
    Ls.post_modal_button_yea_for_del.click()
    Sc().check_delete_post(Ls.name_of_post+'NEW')


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-25 : Удалить закрепленный пост')
@pytest.mark.feed_page
def test_src_25(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    """Закрепляем пост"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_to_fix.click()
    Ls.post_modal_button_yea_for_fix.click()
    Sc().check_fix_post()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-26 : Отменить закрепление поста')
@pytest.mark.feed_page
def test_src_26(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    """Отменяем закрепление поста"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_to_fix.click()
    Ls.post_modal_button_no_for_fix.click()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-27 : Отменить открепление поста')
@pytest.mark.feed_page
def test_src_27(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.add_picture_button.click()
    Ss().put_picture_post(Llk.url_picture_jpg)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    Sc().check_picture_in_post(Ls.count_pictures_in_first_post)
    """Закрепляем пост"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_to_fix.click()
    Ls.post_modal_button_yea_for_fix.click()
    Sc().check_fix_post()
    """Отменить открепление поста"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_to_fix.click()
    Ls.post_modal_button_no_for_fix.click()
    Sc().check_fix_post()


@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-28 : Отменить закрепление поста при уже закрепленном посте')
@pytest.mark.feed_page
def test_src_28(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.add_picture_button.click()
    Ss().put_picture_post(Llk.url_picture_jpg)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    Sc().check_picture_in_post(Ls.count_pictures_in_first_post)
    """Закрепляем пост"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_to_fix.click()
    Ls.post_modal_button_yea_for_fix.click()
    Sc().check_fix_post()
    """Создать второй пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post+'NEW')
    Ls.add_picture_button.click()
    Ss().put_picture_post(Llk.url_picture_png)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    Sc().check_picture_in_post(Ls.count_pictures_in_first_post)
    """Закрепляем второй пост"""
    Ls.first_post_block[1].context_click()
    Ls.post_open_modal[1].click()
    Ls.post_modal_to_fix.click()
    Ls.post_modal_button_no_for_fix.click()
    Sc().check_first_post_fix(Ls.name_of_post)
    """Далее удаляем второй пост"""
    Ls.first_post_block[1].context_click()
    Ls.post_open_modal[1].click()
    Ls.post_modal_delete.click()
    Ls.post_modal_button_yea_for_del.click()
    Sc().check_delete_post(Ls.name_of_post+'NEW')


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-30_1 : Создать пост с применением стиля для текста- жирный')
@pytest.mark.feed_page
def test_src_30_1(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.fat_text_button.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_styles_in_posts_text(Ls.created_post_fat_text)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-30_2 : Создать пост с применением стиля для текста- зачеркнутый ')
@pytest.mark.feed_page
def test_src_30_2(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.cross_line_text_button.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_styles_in_posts_text(Ls.created_post_cross_line_text)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-33 : Создать пост с содержимым: отформатированный текст'
                 'SCR-64 : Удалить свой пост')
@pytest.mark.feed_page
def test_src_33_64(authorization_admin_data):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    """Далее удаляем пост"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_delete.click()
    Ls.post_modal_button_yea_for_del.click()
    Sc().check_delete_post(Ls.name_of_post)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-36 : Создать пост с содержимым: текст с гиперссылкой')
@pytest.mark.feed_page
def test_src_36(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.mark_text_in_create_post.send_keys(Keys.CONTROL, "a")
    Ls.link_text_mod_button.click()
    Ls.link_text_mod_input_field.send_keys(Ls.url_ya_for_check)
    Ls.link_text_mod_button_save.click()
    Ls.published_button.click()
    Sc().check_link_in_posts_text()
    Bs().first_tab()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-37 : Создать пост с содержимым: текст + картинки')
@pytest.mark.feed_page
def test_src_37(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.add_picture_button.click()
    Ss().put_picture_post(Llk.url_picture_jpg)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    Sc().check_picture_in_post(Ls.count_pictures_in_first_post)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-38 : Создать пост с тематикой (хештегом)')
@pytest.mark.feed_page
def test_src_38(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.open_tags_button.click()
    Bs().wait_until_find(Ls.first_tag_in_list)
    tag = Ls.first_tag_in_list.text
    Ls.first_tag_in_list.click()
    Ls.published_button.click()
    Sc().check_tag_in_post(tag)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-41 : Создать пост с содержимым: текст превышающий допустимое кол-во символов')
@pytest.mark.feed_page
def test_src_41(authorization_admin_data):
    Ls.field_text_in_post.click()
    post = As().string_high_low_num_many(10001)
    Ls.field_text_in_post.send_keys(post)
    Ls.published_button.click()
    Sc().check_delete_post(post)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-42 : Создать пост с содержимым: текст + картинки, в кол-ве превышающее допустимое значение.')
@pytest.mark.feed_page
def test_src_42(authorization_admin_data):
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ss().add_pictures(11)
    Sc().check_invalid_count_picture_in_post(11)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-43 : Создать пост с содержимым: текст + картинка, размер которой превышает допустимое значение')
@pytest.mark.feed_page
def test_src_43(authorization_admin_data):
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.add_picture_button.click()
    Ss().put_picture_post(Llk.url_picture_big)
    Sc().check_invalid_count_picture_in_post(1)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-44 : Создать пост с содержимым: картинка (без текста)')
@pytest.mark.feed_page
def test_src_44(authorization_admin_data):
    Ls.field_text_in_post.click()
    Ls.add_picture_button.click()
    Ss().put_picture_post(Llk.url_picture_jpg)
    Sc().check_active_button_published()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-45 : Создать пост с содержимым: несколько пробелов')
@pytest.mark.feed_page
def test_src_45(authorization_admin_data):
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys('   ')
    Sc().check_active_button_published()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-46 : Создать пустой пост')
@pytest.mark.feed_page
def test_src_46(authorization_admin_data):
    Ls.field_text_in_post.click()
    Sc().check_active_button_published()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-47 : Создать пост с содержимым: текст + картинка не валидного формата')
@pytest.mark.feed_page
def test_src_47(authorization_admin_data):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.add_picture_button.click()
    Ss().put_picture_post(Llk.url_picture_invalid)
    """Проверить что картинка не добавилась"""
    Sc().check_invalid_count_picture_in_post(1)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-62 : Редактировать свой пост')
@pytest.mark.feed_page
def test_src_62(authorization_admin_data):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    """Далее редактируем пост"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_edit.click()
    Ls.first_post_edit_text_field.send_keys(Keys.CONTROL, "a")
    Ls.first_post_edit_text_field.send_keys(Ls.name_of_post+'NEW')
    Ls.first_post_edit_text_field_button_ok.click()
    Sc().check_create_post(Ls.name_of_post+'NEW')
    """Далее удаляем пост"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_delete.click()
    Ls.post_modal_button_yea_for_del.click()
    Sc().check_delete_post(Ls.name_of_post + 'NEW')


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-66 : Отменить удаление поста')
@pytest.mark.feed_page
def test_src_66(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    """Отменить удаление поста и проверить что пост остался"""
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_delete.click()
    Ls.post_modal_button_no_for_del.click()
    Sc().check_create_post(Ls.name_of_post)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-204 : Удалить добавленную картинку при создании поста')
@pytest.mark.feed_page
def test_src_204(authorization_admin_data):
    Ls.field_text_in_post.click()
    Ss().add_pictures(3)
    Ls.image_block_in_post_delete_button[2].click()
    Sc().check_invalid_count_picture_in_post(3)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-153 : Создать комментарий содержащий латиницу, кириллицу, цифры и спецсимволы')
@pytest.mark.feed_page
def test_src_153(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    """Создать и проверить комментарий"""
    Ls.post_add_comment_button[0].click()
    comment = As().string_high_low_num_many(30)+Ls.spec_symbols_list
    Ls.comment_input_field.send_keys(comment)
    Ls.add_comment_button.click()
    Sc().check_comment_in_post(comment)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-205 : Удалить свой комментарий')
@pytest.mark.feed_page
def test_src_205(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    """Создать и проверить комментарий"""
    Ls.post_add_comment_button[0].click()
    comment = As().string_high_low_num_many(30)+Ls.spec_symbols_list
    Ls.comment_input_field.send_keys(comment)
    Ls.add_comment_button.click()
    Sc().check_comment_in_post(comment)
    """Удалить комментарий"""
    Ls.delete_comment_button.click()
    Sc().check_delete_comment_in_post()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-206 : Удалить чужой комментарий')
@pytest.mark.feed_page
def test_src_206(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    """Создать и проверить комментарий"""
    Ls.post_add_comment_button[0].click()
    comment = As().string_high_low_num_many(30)+Ls.spec_symbols_list
    Ls.comment_input_field.send_keys(comment)
    Ls.add_comment_button.click()
    Sc().check_comment_in_post(comment)
    """Пере заходим другим пользователем и пытаемся удалить чужой комментарий"""
    Ls.exit_page.click()
    Sc().check_exit()
    Sa().input_login(Vd.front_login_user)
    Sa().input_password(Vd.front_password_user)
    Sa().click_enter()
    Ca().check_auf()
    Sc().check_active_button_on_over_comment_in_post()
    """Далее пере заходим админом для дальнейшего удаления поста"""
    Ls.exit_page.click()
    Sc().check_exit()
    Sa().input_login(Vd.front_login_admin)
    Sa().input_password(Vd.front_password_admin)
    Sa().click_enter()
    Ca().check_auf()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-207 : Развернуть/свернуть список комментариев')
@pytest.mark.feed_page
def test_src_207(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    """Создать 5 комментариев и проверить что они созданы"""
    comments = Ss().add_comments(5)
    Sc().check_many_comments_in_post(comments)
    Sc().check_count_comment_in_post(5)
    Ls.comment_input_field.click()
    Ls.close_comments_list_button.click()
    Sc().check_count_comment_in_post(2)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-208 : Восстановить удаленный комментарий')
@pytest.mark.feed_page
def test_src_208(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    """Создать и проверить комментарий"""
    Ls.post_add_comment_button[0].click()
    comment = As().string_high_low_num_many(30)+Ls.spec_symbols_list
    Ls.comment_input_field.send_keys(comment)
    Ls.add_comment_button.click()
    Sc().check_comment_in_post(comment)
    """Удалить комментарий"""
    Ls.delete_comment_button.click()
    Sc().check_delete_comment_in_post()
    """Восстановить комментарий"""
    Ls.restore_comment_button.click()
    Sc().check_comment_in_post(comment)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-157 : Добавить "Хлопки" к посту')
@pytest.mark.feed_page
def test_src_157(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    """Добавить лайк и проверить что количество увеличилось"""
    count = Ls.first_post_count_of_likes.text
    Ls.first_post_add_like_button.click()
    Sc().check_count_likes_in_post(count)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-159 : Проверить увеличение количества просмотров поста')
@pytest.mark.feed_page
def test_src_159(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    views = Ls.first_post_count_of_views.text
    """Пере заходим другим пользователем и проверяем просмотры"""
    Ls.exit_page.click()
    Sc().check_exit()
    Sa().input_login(Vd.front_login_user)
    Sa().input_password(Vd.front_password_user)
    Sa().click_enter()
    Ca().check_auf()
    Sc().check_count_views_in_post(views)
    """Далее пере заходим админом для дальнейшего удаления поста"""
    Ls.exit_page.click()
    Sc().check_exit()
    Sa().input_login(Vd.front_login_admin)
    Sa().input_password(Vd.front_password_admin)
    Sa().click_enter()
    Ca().check_auf()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-160 : Проверить скролл ленты')
@pytest.mark.feed_page
def test_src_160(authorization_admin_data):
    Bs().wait_until_find(Ls.texts_in_posts)
    count_posts = len(Ls.texts_in_posts)
    Bs().scroll_down()
    Sc().check_count_post_scroll_down(count_posts)
    Bs().scroll_up()
    Sc().check_button_up_when_scroll_up()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-161 : Осуществить скролл ленты вверх, посредством нажатия кнопки "Наверх"')
@pytest.mark.feed_page
def test_src_161(authorization_admin_data):
    Bs().wait_until_find(Ls.texts_in_posts)
    count_posts = len(Ls.texts_in_posts)
    Bs().scroll_down()
    Sc().check_count_post_scroll_down(count_posts)
    Ls.button_up.click()
    Sc().check_button_up_when_scroll_up()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-162 : Поделиться постом ')
@pytest.mark.feed_page
def test_src_162(authorization_admin_data_plus_del_created_post):
    """Создать пост"""
    Ls.field_text_in_post.click()
    Ls.field_text_in_post.send_keys(Ls.name_of_post)
    Ls.published_button.click()
    Sc().check_create_post(Ls.name_of_post)
    """Далее пере заходим для получения ссылки на пост"""
    Ls.exit_page.click()
    Sc().check_exit()
    Sa().input_login(Vd.front_login_user)
    Sa().input_password(Vd.front_password_user)
    Sa().click_enter()
    Ca().check_auf()
    """Далее получаем ссылку и вставляем её в комментарий"""
    Ls.first_post_block[0].click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_share.click()
    Ls.post_add_comment_button[0].click()
    Ls.comment_input_field.send_keys(Keys.CONTROL, "V")
    Ls.add_comment_button.click()
    Sc().check_comment_in_post(Ls.first_post_comments_list[0].text)
    """Далее пере заходим админом для дальнейшего удаления поста"""
    Ls.exit_page.click()
    Sc().check_exit()
    Sa().input_login(Vd.front_login_admin)
    Sa().input_password(Vd.front_password_admin)
    Sa().click_enter()
    Ca().check_auf()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-214 : Создать анонимный опрос с минимальным количеством ответов')
@pytest.mark.feed_page
def test_src_214(authorization_admin_data):
    """Создать опрос"""
    Ls.vote_button.click()
    Ls.vote_question_fild.send_keys(Ls.question_for_vote)
    Ss().add_answers(1)
    Ls.create_vote_button.click()
    Sc().check_question_answers_in_vote(Ls.question_for_vote, 1)
    """Далее удалить опрос"""
    Bs().wait_until_find(Ls.first_post_block)
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_delete.click()
    Ls.post_modal_button_yea_for_del.click()
    Sc().check_delete_vote()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел лента')
@allure.testcase('SCR-215 : Создать анонимный опрос с максимальным количеством ответов (7)')
@pytest.mark.feed_page
def test_src_215(authorization_admin_data):
    """Создать опрос"""
    Ls.vote_button.click()
    Ls.vote_question_fild.send_keys(Ls.question_for_vote)
    Ss().add_answers(7)
    Ls.create_vote_button.click()
    Sc().check_question_answers_in_vote(Ls.question_for_vote, 7)
    """Далее удалить опрос"""
    Bs().wait_until_find(Ls.first_post_block)
    Ls.first_post_block[0].context_click()
    Ls.post_open_modal[0].click()
    Ls.post_modal_delete.click()
    Ls.post_modal_button_yea_for_del.click()
    Sc().check_delete_vote()
