import allure
import pytest
from front.pages.start.start_page import LocatorsStart as Ls
from front.src.helpers.BaseSteps import BaseSteps as Bs
from front.pages.auf.auf_steps import StepsAuf as Sa
from front.src.data.login import ValidData as Vd
from front.pages.apps.apps_checks import ChecksApp as Capp
from front.pages.start.start_checks import StartChecks as Sc
from front.pages.apps.apps_page import LocatorsApps as Lapp


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел приложения')
@allure.testcase('SCR-223 : Перейти к Booking в подразделе сервисы')
@pytest.mark.apps_page
def test_src_223(authorization_user_data):
    Ls.apps_page.click()
    Sc().check_its_tab(Lapp.title_apps, "Приложения")
    Lapp.booking_open_button.click()
    Bs().next_tab()
    Sc().check_its_tab(Lapp.booking_page_title, "Booking.")


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел приложения')
@allure.testcase('SCR-224 : Перейти к Value в подразделе сервисы')
@pytest.mark.apps_page
def test_src_224(authorization_user_data):
    Ls.apps_page.click()
    Sc().check_its_tab(Lapp.title_apps, "Приложения")
    Lapp.value_open_button.click()
    Bs().next_tab()
    Sa().input_login(Vd.front_login_user)
    Sa().input_password(Vd.front_password_user)
    Sa().click_enter()
    Sc().check_its_tab(Lapp.value_page_title, "VALUE.")


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел приложения')
@allure.testcase('SCR-225 : Проверить количество игр на вкладке Игры')
@pytest.mark.apps_page
def test_src_225(authorization_user_data):
    Ls.apps_page.click()
    Sc().check_its_tab(Lapp.title_apps, "Приложения")
    Lapp.game_button.click()
    Capp().check_count_games(5)
