import allure
import pytest
from front.pages.start.start_checks import StartChecks as Sc
from front.pages.start.start_page import LocatorsStartBirthdays as Lb


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Дни рождения')
@allure.testcase('SCR-155 : Перейти в профиль именинника кликнув на его ФИ.')
@pytest.mark.auf
def test_src_155(authorization_user_data):
    Lb.birthday_button_open.click()
    Sc().check_fi_go_lk_birth()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Дни рождения')
@allure.testcase('SCR-156 : Перейти в профиль именинника кликнув на его аватарку.')
@pytest.mark.auf
def test_src_156(authorization_user_data):
    Lb.birthday_button_open.click()
    Sc().check_ava_go_lk_birth()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Дни рождения')
@allure.testcase('SCR-158:Проверить что дни рождения отображаются согласно '
                 'требуемого диапазона и список отображается по нарастающей.')
@pytest.mark.auf
def test_src_158(authorization_user_data):
    Lb.birthday_button_open.click()
    Sc().check_birthdays_go_up()
