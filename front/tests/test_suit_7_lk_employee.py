import allure
import pytest
from front.pages.start.start_page import LocatorsStart as Ls
from front.pages.lk.lk_page import LocatorsLk as Llk
from front.pages.lk.lk_steps import LkSteps as Lks
from front.pages.lk.lk_checks import LkChecks as Lkc
from front.pages.employees.employees_page import LocatorsEmp as Le


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-167 : Добавить на аватарку картинку в формате jpg')
@pytest.mark.lk_employee
def test_src_167(authorization_user_data):
    Ls.fio_main.click()
    old = Lks().get_old_picture(Llk.user_big_ava_lk)
    Llk.user_big_ava_lk.context_click()
    Llk.user_photo_update_button.click()
    Lks().put_picture_big_ava(Llk.url_picture_jpg)
    new = Lks().get_new_picture(Llk.user_big_ava_lk)
    Lkc().check_picture_is_set(old, new)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-168 : Добавить на аватарку картинку в формате webp')
@pytest.mark.lk_employee
def test_src_168(authorization_user_data):
    Ls.fio_main.click()
    old = Lks().get_old_picture(Llk.user_big_ava_lk)
    Llk.user_big_ava_lk.context_click()
    Llk.user_photo_update_button.click()
    Lks().put_picture_big_ava(Llk.url_picture_webp)
    new = Lks().get_new_picture(Llk.user_big_ava_lk)
    Lkc().check_picture_is_set(old, new)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-169 : Добавить на аватарку картинку в формате png')
@pytest.mark.lk_employee
def test_src_169(authorization_user_data):
    Ls.fio_main.click()
    old = Lks().get_old_picture(Llk.user_big_ava_lk)
    Llk.user_big_ava_lk.context_click()
    Llk.user_photo_update_button.click()
    Lks().put_picture_big_ava(Llk.url_picture_png)
    new = Lks().get_new_picture(Llk.user_big_ava_lk)
    Lkc().check_picture_is_set(old, new)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-170 : Добавить на аватарку картинку не валидного формата')
@pytest.mark.lk_employee
def test_src_170(authorization_user_data):
    Ls.fio_main.click()
    old = Lks().get_old_picture(Llk.user_big_ava_lk)
    Llk.user_big_ava_lk.context_click()
    Llk.user_photo_update_button.click()
    Lks().put_picture_big_ava(Llk.url_picture_invalid)
    new = Lks().get_new_picture(Llk.user_big_ava_lk)
    Lkc().check_picture_not_set(old, new)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-171 : Добавить на аватарку картинку превышающую максимально допустимый вес')
@pytest.mark.lk_employee
def test_src_171(authorization_user_data):
    Ls.fio_main.click()
    old = Lks().get_old_picture(Llk.user_big_ava_lk)
    Llk.user_big_ava_lk.context_click()
    Llk.user_photo_update_button.click()
    Lks().put_picture_big_ava(Llk.url_picture_big)
    new = Lks().get_new_picture(Llk.user_big_ava_lk)
    Lkc().check_picture_not_set(old, new)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-174 : Удалить аватарку')
@pytest.mark.lk_employee
def test_src_173(authorization_user_data):
    Ls.fio_main.click()
    old = Lks().get_old_picture(Llk.user_big_ava_lk)
    Llk.user_big_ava_lk.context_click()
    Llk.user_photo_delete_button.click()
    Llk.modal_button_change_or_yes.click()
    new = Lks().get_new_picture(Llk.user_big_ava_lk)
    Lkc().check_picture_is_del(old, new)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-174 : Удалить аватарку')
@pytest.mark.lk_employee
def test_src_174(authorization_user_data):
    Ls.fio_main.click()
    old = Lks().get_old_picture(Llk.user_big_ava_lk)
    Llk.user_big_ava_lk.context_click()
    Llk.user_photo_delete_button.click()
    Llk.modal_button_delete_or_cancel.click()
    new = Lks().get_new_picture(Llk.user_big_ava_lk)
    Lkc().check_picture_is_del(old, new)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-175 : Добавить на обложку картинку формата jpg')
@pytest.mark.lk_employee
def test_src_175(authorization_user_data):
    Ls.fio_main.click()
    old = Lks().get_old_picture(Llk.user_background)
    Llk.user_background.click()
    Llk.modal_button_change_or_yes.click()
    Lks().put_picture_background(Llk.url_picture_jpg)
    new = Lks().get_new_picture(Llk.user_background)
    Lkc().check_picture_is_set(old, new)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-176 : Добавить на обложку картинку формата webp')
@pytest.mark.lk_employee
def test_src_176(authorization_user_data):
    Ls.fio_main.click()
    old = Lks().get_old_picture(Llk.user_background)
    Llk.user_background.click()
    Llk.modal_button_change_or_yes.click()
    Lks().put_picture_background(Llk.url_picture_webp)
    new = Lks().get_new_picture(Llk.user_background)
    Lkc().check_picture_is_set(old, new)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-177 : Добавить на обложку картинку формата png')
@pytest.mark.lk_employee
def test_src_177(authorization_user_data):
    Ls.fio_main.click()
    old = Lks().get_old_picture(Llk.user_background)
    Llk.user_background.click()
    Llk.modal_button_change_or_yes.click()
    Lks().put_picture_background(Llk.url_picture_png)
    new = Lks().get_new_picture(Llk.user_background)
    Lkc().check_picture_is_set(old, new)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-178 : Добавить на обложку картинку не валидного формата')
@pytest.mark.lk_employee
def test_src_178(authorization_user_data):
    Ls.fio_main.click()
    old = Lks().get_old_picture(Llk.user_background)
    Llk.user_background.click()
    Llk.modal_button_change_or_yes.click()
    Lks().put_picture_background(Llk.url_picture_invalid)
    new = Lks().get_new_picture(Llk.user_background)
    Lkc().check_picture_not_set(old, new)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-179 : Добавить на обложку картинку превышающую максимально допустимый вес')
@pytest.mark.lk_employee
def test_src_179(authorization_user_data):
    Ls.fio_main.click()
    old = Lks().get_old_picture(Llk.user_background)
    Llk.user_background.click()
    Llk.modal_button_change_or_yes.click()
    Lks().put_picture_background(Llk.url_picture_big)
    new = Lks().get_new_picture(Llk.user_background)
    Lkc().check_picture_not_set(old, new)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-180 : Удалить обложку')
@pytest.mark.lk_employee
def test_src_180(authorization_user_data):
    Ls.fio_main.click()
    old = Lks().get_old_picture(Llk.user_background)
    Llk.user_background.click()
    Llk.modal_button_delete_or_cancel.click()
    new = Lks().get_new_picture(Llk.user_background)
    Lkc().check_picture_is_del(old, new)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-181 : Изменить картинку чужой аватарки')
@pytest.mark.lk_employee
def test_src_181(authorization_user_data):
    Ls.employees_page.click()
    Le.fio_employees[1].click()
    Lkc().check_else_picture_del_upload(Llk.user_photo_update_button)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-182 : Удалить чужую аватарку')
@pytest.mark.lk_employee
def test_src_182(authorization_user_data):
    Ls.employees_page.click()
    Le.fio_employees[1].click()
    Lkc().check_else_picture_del_upload(Llk.user_photo_delete_button)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-185 : Перейти к telegram пользователя через ссылку в профиле')
@pytest.mark.lk_employee
def test_src_185(authorization_user_data):
    Ls.fio_main.click()
    Llk.telegram.click()
    Lkc().check_go_telegram()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-187 : Добавить на аватарку файл не валидного формата')
@pytest.mark.lk_employee
def test_src_187(authorization_user_data):
    Ls.fio_main.click()
    old = Lks().get_old_picture(Llk.user_big_ava_lk)
    Llk.user_big_ava_lk.context_click()
    Llk.user_photo_update_button.click()
    Lks().put_picture_big_ava(Llk.url_invalid_file)
    new = Lks().get_new_picture(Llk.user_big_ava_lk)
    Lkc().check_picture_not_set(old, new)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-188 : Добавить на обложку файл не валидного формата')
@pytest.mark.lk_employee
def test_src_188(authorization_user_data):
    Ls.fio_main.click()
    old = Lks().get_old_picture(Llk.user_background)
    Llk.user_background.click()
    Llk.modal_button_change_or_yes.click()
    Lks().put_picture_background(Llk.url_invalid_file)
    new = Lks().get_new_picture(Llk.user_background)
    Lkc().check_picture_not_set(old, new)
