import allure
import pytest
from front.pages.creators.creators_checks import CreatorsChecks as Cc
from front.src.helpers.BaseSteps import BaseSteps as Bs
from front.pages.start.start_page import LocatorsStart as Ls
from front.pages.creators.creators_page import LocatorsCreators as Lc


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел создатели')
@allure.testcase('SCR-163 : Перейти в профиль создателя кликнув на ФИ.')
@pytest.mark.creators_page
def test_src_163(authorization_user_data):
    Ls.creators_page.click()
    Cc().check_go_creators()
    Cc().check_fi_go_lk()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел создатели')
@allure.testcase('SCR-164 : Проверить убирание/возвращение блоков интерфейса при скролле')
@pytest.mark.creators_page
def test_src_164(authorization_user_data):
    Ls.creators_page.click()
    Cc().check_go_creators()
    Bs().scroll_down()
    Bs().wait_until_find(Lc.down_title_creators.is_displayed())
    Cc().check_blocks_out()
    Bs().scroll_up()
    Bs().wait_until_find(Lc.title_creators.is_displayed())
    Cc().check_blocks_back()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел создатели')
@allure.testcase('SCR-210 : Проверить список создателей')
@pytest.mark.creators_page
def test_src_210(authorization_user_data):
    Ls.creators_page.click()
    Cc().check_go_creators()
    Cc().check_creators_list()
