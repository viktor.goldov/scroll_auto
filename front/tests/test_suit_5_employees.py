import allure
import pytest
from front.pages.start.start_page import LocatorsStart as Ls
from front.pages.employees.employees_checks import EmployeesChecks as Ems


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-73 : Отфильтровать сотрудников по городу')
@pytest.mark.employees_page
def test_src_73(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_cities_filter()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-74 : Отфильтровать сотрудников по направлению')
@pytest.mark.employees_page
def test_src_74(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_department_filter()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-75 : Отфильтровать сотрудников по городу и направлению')
@pytest.mark.employees_page
def test_src_75(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_cities_department_filter()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-76 : Отфильтровать сотрудников по Фамилии')
@pytest.mark.employees_page
def test_src_76(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_by_last_name()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-77 : Отфильтровать сотрудников по Имени')
@pytest.mark.employees_page
def test_src_77(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_by_first_name()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-78 : Отфильтровать сотрудников по Имени и Городу')
@pytest.mark.employees_page
def test_src_78(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_by_first_name_city_filter()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-79 : Отфильтровать сотрудников по Имени и Направлению')
@pytest.mark.employees_page
def test_src_79(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_by_first_name_depart_filter()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-80 : Отфильтровать сотрудников по Фамилии и Городу')
@pytest.mark.employees_page
def test_src_80(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_by_last_name_city_filter()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-81 : Отфильтровать сотрудников по Фамилии и Направлению')
@pytest.mark.employees_page
def test_src_81(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_by_last_name_depart_filter()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-82 : Отфильтровать сотрудников по Имени, Городу и Направлению')
@pytest.mark.employees_page
def test_src_82(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_by_first_name_city_depart_filter()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-83 : Отфильтровать сотрудников по Фамилии, Городу и Направлению')
@pytest.mark.employees_page
def test_src_83(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_by_last_name_city_depart_filter()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-84 : Найти сотрудника по Имени + Фамилии')
@pytest.mark.employees_page
def test_src_84(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_by_name_lastname()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-85 : Найти сотрудника по Фамилии + Имени')
@pytest.mark.employees_page
def test_src_85(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_by_lastname_name()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-86 : Отфильтровать сотрудников по Фамилии + несколько пробелов в начале')
@pytest.mark.employees_page
def test_src_86(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_by_last_name_spaces()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-87 : Отфильтровать сотрудников по Имени + несколько пробелов в начале')
@pytest.mark.employees_page
def test_src_87(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_by_first_name_spaces()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-88 : Поиск сотрудника по Фамилии + Имени + несколько пробелов в начале и в конце')
@pytest.mark.employees_page
def test_src_88(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_by_name_lastname_spaces()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-89 : Перейти на карточку сотрудника кликнув на аватар')
@pytest.mark.employees_page
def test_src_89(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_ava_go_lk()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-90 : Перейти на карточку сотрудника кликнув на ФИ')
@pytest.mark.employees_page
def test_src_90(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_fi_go_lk()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Раздел сотрудники')
@allure.testcase('SCR-222 : Поиск с использованием буквы "е" для поиска сотрудника с ФИ содержащие букву "ё"')
@pytest.mark.employees_page
def test_src_222(authorization_user_data):
    Ls.employees_page.click()
    Ems().check_e_in_fullname()
