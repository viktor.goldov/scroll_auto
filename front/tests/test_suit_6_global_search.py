import allure
import pytest
from front.pages.start.start_page import LocatorsStart as Ls
from front.pages.start.start_checks import StartChecks as Sc
from front.pages.start.start_steps import StartSteps as Ss
from front.pages.employees.employees_page import LocatorsEmp as Le


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Глобальный поиск')
@allure.testcase('SCR-128 : Найти пост содержащий символы латиницы')
@pytest.mark.global_search
def test_src_128(authorization_user_data):
    Ls.search_field.click()
    Ls.search_field.send_keys(Ls.eng_key)
    Ls.rsdsf_first_post_eng_num_sym.click()
    Sc().check_post_has_some_sym(Ls.eng_key)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Глобальный поиск')
@allure.testcase('SCR-129 : Найти пост содержащий символы кириллицы')
@pytest.mark.global_search
def test_src_129(authorization_user_data):
    Ls.search_field.click()
    Ls.search_field.send_keys(Ls.rus_key)
    Ls.rsdsf_first_post_rus.click()
    Sc().check_post_has_some_sym(Ls.rus_key)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Глобальный поиск')
@allure.testcase('SCR-130 : Найти пост содержащий цифры')
@pytest.mark.global_search
def test_src_130(authorization_user_data):
    Ls.search_field.click()
    Ls.search_field.send_keys(Ls.num_key)
    Ls.rsdsf_first_post_eng_num_sym.click()
    Sc().check_post_has_some_sym(Ls.num_key)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Глобальный поиск')
@allure.testcase('SCR-131 : Найти пост содержащий спецсимволы')
@pytest.mark.global_search
def test_src_131(authorization_user_data):
    Ls.search_field.click()
    Ls.search_field.send_keys(Ls.spec_key)
    Ls.rsdsf_first_post_eng_num_sym.click()
    Sc().check_post_has_some_sym(Ls.spec_key)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Глобальный поиск')
@allure.testcase('SCR-132 : Осуществить поиск сотрудников по фамилии +'
                 ' SCR-138 : Перейти в профиль сотрудника используя список предварительного поиска')
@pytest.mark.global_search
def test_src_132_138(authorization_user_data):
    last_name = Ss().get_last_name_main()
    Ls.search_field.click()
    Ls.search_field.send_keys(last_name)
    Ls.rsdsf_first_employee.click()
    Sc().check_last_name_go_lk_rsdsf(last_name)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Глобальный поиск')
@allure.testcase('SCR-133 : Осуществить поиск сотрудников по имени')
@pytest.mark.global_search
def test_src_133(authorization_user_data):
    name = Ss().get_name_main()
    Ls.search_field.click()
    Ls.search_field.send_keys(name)
    Ls.rsdsf_first_employee.click()
    Sc().check_name_go_lk_rsdsf(name)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Глобальный поиск')
@allure.testcase('SCR-134 : Осуществить поиск сотрудника по фамилии и имени с пробелами в начале и в конце запроса')
@pytest.mark.global_search
def test_src_134(authorization_user_data):
    full_name = Ss().get_full_name_main()
    Ls.search_field.click()
    Ls.search_field.send_keys("    "+full_name+"     ")
    Ls.rsdsf_first_employee.click()
    Sc().check_full_name_go_lk_rsdsf(full_name)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Глобальный поиск')
@allure.testcase('SCR-135 : Осуществить поиск постов по тегу')
@pytest.mark.global_search
def test_src_135(authorization_user_data):
    Ls.search_field.click()
    Ls.search_field.send_keys(Ls.tag)
    Ls.rsdsf_first_tag.click()
    Sc().check_tag_go_post_rsdsf(Ls.tag)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Глобальный поиск')
@allure.testcase('SCR-136 : Осуществить поиск введя в поле несколько пробелов')
@pytest.mark.global_search
def test_src_136(authorization_user_data):
    Ls.search_field.click()
    Ls.search_field.send_keys('   ')
    Sc().check_have_some('   ')


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Глобальный поиск')
@allure.testcase('SCR-137 : Осуществить поиск при пустом поле ввода')
@pytest.mark.global_search
def test_src_137(authorization_user_data):
    Ls.search_field.click()
    Ls.search_field.press_enter()
    Sc().check_have_some('')


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Глобальный поиск')
@allure.testcase('SCR-139 : Перейти к списку сотрудников используя список предварительного поиска')
@pytest.mark.global_search
def test_src_139(authorization_user_data):
    Ls.search_field.click()
    Ls.search_field.send_keys(Ls.rus_key)
    Ls.rsdsf_employees.click()
    Sc().check_its_tab(Le.title_employees, "Сотрудники")


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Глобальный поиск')
@allure.testcase('SCR-140 : Перейти к списку постов используя список предварительного поиска')
@pytest.mark.global_search
def test_src_140(authorization_user_data):
    Ls.search_field.click()
    Ls.search_field.send_keys(Ls.eng_key)
    Ls.rsdsf_posts.click()
    Sc().check_its_tab(Ls.post_page_title, "Посты")
