import allure
import pytest
from front.src.helpers.BaseSteps import BaseSteps as Bs
from front.pages.start.start_page import LocatorsStart as Ls
from front.pages.start.start_checks import StartChecks as Sc
from front.pages.employees.employees_page import LocatorsEmp as Le
from front.pages.geography.geo_page import LocatorsGeo as Lg
from front.pages.settings.settings_page import LocatorsSettings as Lset
from front.pages.creators.creators_page import LocatorsCreators as Lc
from front.pages.apps.apps_page import LocatorsApps as Lapp
from front.pages.welcome_wiki_send_your.welcome_wiki_send_your import LocatorsWelcomeWikiSend as Lw


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Основное меню')
@allure.testcase('SCR-141 : Перейти в профиль сотрудника нажав на ФИО')
@pytest.mark.main_menu
def test_src_141(authorization_user_data):
    Ls.fio_main.click()
    Sc().check_fi_go_lk()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Основное меню')
@allure.testcase('SCR-142 : Перейти в профиль сотрудника нажав на аватарку')
@pytest.mark.main_menu
def test_src_142(authorization_user_data):
    Ls.ava_main.click()
    Sc().check_ava_go_lk()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Основное меню')
@allure.testcase('SCR-143 : Перейти в раздел "Лента"')
@pytest.mark.main_menu
@pytest.mark.smoke
def test_src_143(authorization_user_data):
    Ls.feed.click()
    Sc().check_its_feed()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Основное меню')
@allure.testcase('SCR-144 : Перейти в раздел "Сотрудники"')
@pytest.mark.main_menu
@pytest.mark.smoke
def test_src_144(authorization_user_data):
    Ls.employees_page.click()
    Sc().check_its_tab(Le.title_employees, "Сотрудники")


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Основное меню')
@allure.testcase('SCR-145 : Перейти в раздел "География"')
@pytest.mark.main_menu
@pytest.mark.smoke
def test_src_145(authorization_user_data):
    Ls.geolocation_page.click()
    Sc().check_its_tab(Lg.title_geo, "География")


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Основное меню')
@allure.testcase('SCR-146 : Перейти в раздел "Настройки"')
@pytest.mark.main_menu
@pytest.mark.smoke
def test_src_146(authorization_user_data):
    Ls.settings_page.click()
    Sc().check_its_tab(Lset.title_settings, "Настройки")


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Основное меню')
@allure.testcase('SCR-148 : Перейти к Welcome презентации')
@pytest.mark.main_menu
@pytest.mark.smoke
def test_src_148(authorization_user_data):
    Ls.welcome_page.click()
    Bs().next_tab()
    Sc().check_its_tab(Lw.title_welcome_wiki, "Yandex Wiki")


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Основное меню')
@allure.testcase('SCR-149 : Перейти к Wiki')
@pytest.mark.main_menu
@pytest.mark.smoke
def test_src_149(authorization_user_data):
    Ls.wiki_page.click()
    Bs().next_tab()
    Sc().check_its_tab(Lw.title_welcome_wiki, "Yandex Wiki")


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Основное меню')
@allure.testcase('SCR-150 : Проверить появление всплывающего окна - отправка пожелания')
@pytest.mark.main_menu
@pytest.mark.smoke
def test_src_150(authorization_user_data):
    Ls.send_your_mind_page.click()
    Lw.button_cancel_modal_window.click()
    Sc().check_its_feed()
    Ls.send_your_mind_page.click()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Основное меню')
@allure.testcase('SCR-151 : Перейти в раздел "Создатели"')
@pytest.mark.main_menu
@pytest.mark.smoke
def test_src_151(authorization_user_data):
    Ls.creators_page.click()
    Sc().check_its_tab(Lc.title_page_creators, "Создатели")


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Основное меню')
@allure.testcase('SCR-152 : Проверить выход из соцсети нажав Выйти')
@pytest.mark.main_menu
@pytest.mark.smoke
def test_src_152(authorization_user_data):
    Ls.exit_page.click()
    Sc().check_exit()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Основное меню')
@allure.testcase('SCR-151 : Перейти в раздел "Приложения"')
@pytest.mark.main_menu
@pytest.mark.smoke
def test_src_151(authorization_user_data):
    Ls.apps_page.click()
    Sc().check_its_tab(Lapp.title_apps, "Приложения")
