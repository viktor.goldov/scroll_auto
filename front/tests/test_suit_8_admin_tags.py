import allure
import pytest
from front.pages.start.start_page import LocatorsStart as Ls
from front.pages.admin.admin_page import LocatorsAdmin as La
from front.pages.admin.admin_steps import AdminSteps as As
from front.pages.admin.admin_checks import AdminChecks as Ac


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-189 : Добавить тематику содержащую кириллицу в верхнем и нижнем регистре + цифры')
@pytest.mark.admin
def test_src_189(authorization_admin_data):
    Ls.admin_page.click()
    letters = As().string_high_low_num_rus()
    La.admin_search_field.send_keys(letters)
    La.add_tag_button.click()
    Ac().check_create_tag(letters)
    As().delete_tag(letters)
    Ac().check_not_create_tag(letters)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-190 : Добавить тематику содержащую латиницу в верхнем, нижнем регистре + разрешенные спецсимволы')
@pytest.mark.admin
def test_src_190(authorization_admin_data):
    Ls.admin_page.click()
    letters = As().string_high_low_num_en()
    La.admin_search_field.send_keys(letters)
    La.add_tag_button.click()
    Ac().check_create_tag(letters)
    As().delete_tag(letters)
    Ac().check_not_create_tag(letters)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-191 : Добавить тематику состоящую из 1 символа')
@pytest.mark.admin
def test_src_191(authorization_admin_data):
    Ls.admin_page.click()
    letters = 'W'
    La.admin_search_field.send_keys(letters)
    La.add_tag_button.click()
    Ac().check_create_tag(letters)
    As().delete_tag(letters)
    Ac().check_not_create_tag(letters)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-192 : Добавить тематику содержащую не валидные символы')
@pytest.mark.admin
def test_src_192(authorization_admin_data):
    Ls.admin_page.click()
    Ac().check_invalid_symbols_insert()


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-193 : Добавить тематику с содержанием - несколько пробелов')
@pytest.mark.admin
def test_src_193(authorization_admin_data):
    Ls.admin_page.click()
    letters = '       '
    La.admin_search_field.send_keys(letters)
    La.add_tag_button.click()
    Ac().check_not_create_tag(letters)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-194 : Добавить тематику при пустом поле ввода')
@pytest.mark.admin
def test_src_194(authorization_admin_data):
    Ls.admin_page.click()
    letters = ''
    La.admin_search_field.send_keys(letters)
    La.add_tag_button.click()
    Ac().check_not_create_tag(letters)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-195 : Удалить тематику')
@pytest.mark.admin
def test_src_195(authorization_admin_data):
    """Последовательно создаем и далее удаляем тэг"""
    Ls.admin_page.click()
    letters = 'Delete'
    La.admin_search_field.send_keys(letters)
    La.add_tag_button.click()
    As().delete_tag(letters)
    Ac().check_not_create_tag(letters)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-196 : Добавить тематику состоящую из 255 символов')
@pytest.mark.admin
def test_src_196(authorization_admin_data):
    Ls.admin_page.click()
    letters = As().string_high_low_num_many(255)
    La.admin_search_field.send_keys(letters)
    La.add_tag_button.click()
    Ac().check_create_tag_up_68(letters)
    As().delete_tag(letters)
    Ac().check_not_create_tag(letters)


@allure.feature('Функциональное тестирование')
@allure.story('Irlix_scroll: Профиль сотрудника')
@allure.testcase('SCR-202 : Добавить тематику состоящую из >255 символов')
@pytest.mark.admin
def test_src_202(authorization_admin_data):
    Ls.admin_page.click()
    letters = As().string_high_low_num_many(256)
    La.admin_search_field.send_keys(letters)
    La.add_tag_button.click()
    Ac().check_not_create_tag_up_68(letters)
