from front.pages.auf.auf_page import LocatorsAuf as La
import allure


class StepsAuf:

    @allure.step("Ввести логин")
    def input_login(self, login):
        La.login_field.click().send_keys(login)

    @allure.step("Ввести пароль")
    def input_password(self, password):
        La.password_field.click().send_keys(password)

    @allure.step("Нажать Поехали")
    def click_enter(self):
        La.enter_button.click()

    @allure.step("Нажать Запомнить")
    def click_remember(self):
        La.remember_me.click()
