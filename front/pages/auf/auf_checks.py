from front.pages.auf.auf_page import LocatorsAuf as La
from front.pages.start.start_page import LocatorsStart as Ls
import allure


class ChecksAuf:

    @allure.step("Проверить что авторизация успешна")
    def check_auf(self):
        assert Ls.start_title.is_enabled(), 'Авторизация не успешна'

    @allure.step("Проверить что авторизация НЕ успешна")
    def check_auf_bad(self):
        assert La.main_title.is_displayed(), 'Авторизация успешна, должна быть НЕ успешна'
