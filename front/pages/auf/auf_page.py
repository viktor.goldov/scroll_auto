from selene.api import *


class LocatorsAuf:
    login_field = s(by.xpath('//*[@id="username"]'))
    password_field = s(by.xpath('//*[@id="password"]'))
    remember_me = s(by.xpath('//*[@id="rememberMe"]'))
    enter_button = s(by.xpath('//*[@id="kc-login"]'))
    main_title = s(by.text('Irlix'))
