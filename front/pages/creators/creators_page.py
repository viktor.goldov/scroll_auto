from selene.api import *


class LocatorsCreators:
    creators_fio = ss(by.class_name('i-creators-item__name-link'))
    title_page_creators = s(by.class_name('i-creators__title'))
    down_page_creators = s(by.xpath('//p[2]'))
    title_creators = s(by.text('.//*[text()[normalize-space(.) = concat("", "Создатели")]]'))
    down_title_creators = s(by.text('.//*[text()[normalize-space(.) = concat("", "Designed by Irlix in Russia")]]'))
