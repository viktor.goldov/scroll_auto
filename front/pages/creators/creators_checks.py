import allure
from selene.api import *
from front.pages.creators.creators_page import LocatorsCreators as Lc
from front.pages.start.start_page import LocatorsStart as Ls
from creators_list import *


class CreatorsChecks:
    @allure.step("Проверить переход на вкладку Создатели")
    def check_go_creators(self):
        assert Lc.title_page_creators.text == 'Создатели', 'Переход на страницу Создатели не успешен'

    @allure.step("Проверить переход по клику на ФИ в лк юзера")
    def check_fi_go_lk(self):
        fio = Lc.creators_fio
        print(len(fio))
        for i in range(len(fio)):
            user = fio[i].text
            fio[i].click()
            assert s(by.text(user)), f'Переход с экрана Создатели в личный кабинет пользователя {user} ' \
                                     f'НЕ осуществлен'
            Ls.creators_page.click()

    @allure.step("Проверить все ли создатели на месте")
    def check_creators_list(self):
        fio = Lc.creators_fio
        for i in range(len(fio)):
            user = fio[i].text
            if user in creators:
                creators.remove(user)
            else:
                assert False, f'{user} не в списке'
        assert len(creators) == 0, f"Список не верный, вот кого не хватает {creators}"

    @allure.step("Проверить убирание блоков интерфейса при скролле")
    def check_blocks_out(self):
        assert Ls.right_info_block.is_displayed() is False \
               and Ls.left_info_block.is_displayed() is False, 'Блоки НЕ убираются с экрана'

    @allure.step("Проверить возвращение блоков интерфейса при скролле")
    def check_blocks_back(self):
        assert Ls.right_info_block.is_displayed() and Ls.left_info_block.is_displayed(), 'Блоки не возвращаются'
