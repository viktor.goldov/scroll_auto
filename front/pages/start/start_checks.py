import datetime
import allure
from front.pages.start.start_page import LocatorsStartBirthdays as Lb
from front.pages.start.start_page import LocatorsStart as Ls
from front.pages.auf.auf_page import LocatorsAuf as La
from front.pages.lk.lk_page import LocatorsLk as Lk
from front.src.helpers.BaseSteps import BaseSteps as Bs


class StartChecks:
    @allure.step("Проверить переход по клику на Аву в лк юзера - дни рождения")
    def check_ava_go_lk_birth(self):
        ava = Lb.birthdays_ava
        for i in range(len(ava)):
            ava[i].click()
            user = ava[i].get_attribute('href').split('/')
            lk_user = Lk.email.text.split('@')
            assert user[4] == lk_user[0], f'Переход с главного экрана в личный кабинет пользователя ' \
                                          f'{Lb.birthdays_fio[i].text} НЕ осуществлен'

    @allure.step("Проверить переход по клику на ФИ в лк юзера - дни рождения")
    def check_fi_go_lk_birth(self):
        fio = Lb.birthdays_fio
        for i in range(len(fio)):
            fio[i].click()
            user = fio[i].get_attribute('href').split('/')
            lk_user = Lk.email.text.split('@')
            assert user[4] == lk_user[0], f'Переход с главного экрана в личный кабинет пользователя {fio[i].text} ' \
                                          f'НЕ осуществлен'

    @allure.step("Проверить переход по клику на Аву в лк юзера")
    def check_ava_go_lk(self):
        ava = Ls.ava_main
        ava.click()
        user = ava.get_attribute('href').split('/')
        lk_user = Lk.email.text.split('@')
        assert user[4] == lk_user[0], f'Переход с главного экрана в личный кабинет пользователя ' \
                                      f'{Ls.fio_main.text} НЕ осуществлен'

    @allure.step("Проверить переход по клику на ФИ в лк юзера")
    def check_fi_go_lk(self):
        fio = Ls.fio_main
        fio.click()
        user = fio.get_attribute('href').split('/')
        lk_user = Lk.email.text.split('@')
        assert user[4] == lk_user[0], f'Переход с главного экрана в личный кабинет пользователя {fio.text} ' \
                                      f'НЕ осуществлен'

    @allure.step("Проверить что список отображается по нарастающей")
    def check_birthdays_go_up(self):
        dates = Lb.birthdays_date
        lis_days = []
        lis_months = []
        lis_days_second = []
        lis_months_second = []
        now = datetime.datetime.now()
        name = Lb.birthdays_fio
        count = 0
        month = {'января': 1, 'февраля': 2, 'марта': 3, 'апреля': 4, 'мая': 5, 'июня': 6, 'июля': 7, 'августа': 8,
                 'сентября': 9, 'октября': 10, 'ноября': 11, 'декабря': 12}
        """Составляется диапазон в соответствии с которым будут проводиться проверки"""
        if str(now.month) in '1, 3, 5, 7, 8, 10, 12':
            while count != 31:
                for i in range(now.day, 32):
                    lis_days.append(i)
                    lis_months.append(now.month)
                    count += 1
                for j in range(1, 32):
                    lis_days_second.append(j)
                    lis_months_second.append(now.month + 1)
                    count += 1
                    if count == 31:
                        break
        elif str(now.month) in '4, 6, 9, 11':
            while count != 31:
                for i in range(now.day, 31):
                    lis_days.append(i)
                    lis_months.append(now.month)
                    count += 1
                for j in range(1, 31):
                    lis_days_second.append(j)
                    lis_months_second.append(now.month + 1)
                    count += 1
                    if count == 31:
                        break
        else:
            while count != 31:
                for i in range(now.day, 29):
                    lis_days.append(i)
                    lis_months.append(now.month)
                    count += 1
                for j in range(1, 29):
                    lis_days_second.append(j)
                    lis_months_second.append(now.month + 1)
                    count += 1
                    if count == 31:
                        break
        """Проверить что в список попадают согласно требованиям и список отображается по нарастающей"""
        months_up = 0
        days_up = 0
        days_second_up = 0
        for i in range(len(dates)):
            trash = dates[i].text.split(' ')
            if month.get(trash[1]) in lis_months and int(trash[0]) in lis_days:
                if months_up <= month.get(trash[1]) and days_up <= int(trash[0]):
                    months_up = month.get(trash[1])
                    days_up = int(trash[0])
                    continue
                else:
                    assert False, f'День рождения пользователя {name[i].text} идет не по порядку'
            elif month.get(trash[1]) in lis_months_second and int(trash[0]) in lis_days_second:
                if months_up <= month.get(trash[1]) and days_second_up <= int(trash[0]):
                    months_up = month.get(trash[1])
                    days_second_up = int(trash[0])
                    continue
                else:
                    assert False, f'День рождения пользователя {name[i].text} идет не по порядку'
            else:
                assert False, f'День рождения пользователя {name[i].text}  не в диапазоне'

    @allure.step("Проверить что клиент на вкладке Лента")
    def check_its_feed(self):
        assert Ls.start_title.is_enabled(), 'Переход на вкладку Лента НЕ осуществлен'

    @allure.step("Проверить что клиент на вкладке")
    def check_its_tab(self, tab, name):
        assert tab.text == name, f'Переход на вкладку {name} НЕ осуществлен'

    @allure.step("Проверить выход из соцсети успешен - нажатие на Выйти")
    def check_exit(self):
        assert La.main_title.is_displayed(), 'Пользователь не вышел из соцсети'

    @allure.step("Проверить что найденный в глобальном поиске пост содержит определённые символы")
    def check_post_has_some_sym(self, element):
        list_text = []
        count = 0
        Bs().wait_until_find(Ls.post_text)
        for i in range(len(Ls.post_text)):
            list_text.append(Ls.post_text[i].text)
        text = ''.join(list_text)
        symbols = list(text)
        for j in range(len(symbols)):
            sym = str(symbols[j]).lower()
            if element == sym:
                count += 1
                break
        assert count > 0, f'В найденном посте не найдено символа {element}'

    @allure.step("Проверить переход в лк юзера, из глобального поиска по фамилии")
    def check_last_name_go_lk_rsdsf(self, employee):
        Bs().wait_until_find(Lk.name.text)
        lk_user = Lk.name.text.split(' ')
        assert employee == lk_user[0], f'Переход с глобального поиска в личный кабинет пользователя {employee} ' \
                                       f'НЕ осуществлен'

    @allure.step("Проверить переход в лк юзера, из глобального поиска по имени")
    def check_name_go_lk_rsdsf(self, employee):
        Bs().wait_until_find(Lk.name.text)
        lk_user = Lk.name.text.split(' ')
        assert employee == lk_user[1], f'Переход с глобального поиска в личный кабинет пользователя {employee} ' \
                                       f'НЕ осуществлен'

    @allure.step("Проверить переход в лк юзера, из глобального поиска по Фамилии и имени")
    def check_full_name_go_lk_rsdsf(self, employee):
        Bs().wait_until_find(Lk.name.text)
        assert employee == Lk.name.text, f'Переход с глобального поиска в личный кабинет пользователя {employee} ' \
                                         f'НЕ осуществлен'

    @allure.step("Проверить переход из глобального поиска по тегу на пост с тегом")
    def check_tag_go_post_rsdsf(self, tag):
        Bs().wait_until_find(Ls.tag_in_post_global_search)
        for i in range(len(Ls.tag_in_post_global_search)):
            assert tag == Ls.tag_in_post_global_search[i].text, \
                f'В посте найдет тэг - {Ls.tag_in_post_global_search[i].text} НЕ соответствующий тэгу в поиске - {tag}'

    @allure.step("Проверить если что то в поиске введя что-то")
    def check_have_some(self, element):
        Bs().wait_until_find(Ls.rsdsf_first_element)
        assert not Ls.rsdsf_first_element.is_enabled(), \
            f'Введя данные - {element},  поиск не пуст, в поиске есть {Ls.rsdsf_first_element.text}'

    @allure.step("Проверить применились ли свойства к тексту в созданном посте")
    def check_styles_in_posts_text(self, element):
        Bs().wait_until_find(Ls.feed)
        assert element.is_enabled(), "Свойство не применено"

    @allure.step("Проверить прикрепилась ли ссылка к тексту в созданном посте")
    def check_link_in_posts_text(self):
        Bs().wait_until_find(Ls.feed)
        Ls.texts_in_first_post.click()
        Bs().next_tab()
        assert Ls.ya_title_for_check.get_attribute('aria-label') == 'Яндекс', \
            "Переход по прикрепленной ссылке НЕ осуществлен"

    @allure.step("Проверить НЕ создался ли или удалился ли пост")
    def check_delete_post(self, name):
        Bs().wait_until_find(Ls.feed)
        for i in range(len(Ls.texts_in_posts)):
            assert Ls.texts_in_posts[i].text != name, f"Пост с названием - {name} не удален"

    @allure.step("Проверить удалился ли опрос")
    def check_delete_vote(self):
        Bs().wait_until_find(Ls.feed)
        assert not Ls.vote_title.is_enabled(), f"Опрос не удален"

    @allure.step("Проверить создался ли пост")
    def check_create_post(self, name):
        Bs().wait_until_find(Ls.texts_in_posts)
        count = 0
        for i in range(len(Ls.texts_in_posts)):
            if Ls.texts_in_posts[i].text == name:
                count += 1
            else:
                continue
        assert count > 0, f"Пост с названием - {name} не создан"

    @allure.step("Проверить создался ли пост с тэгом")
    def check_tag_in_post(self, tag):
        Bs().wait_until_find(Ls.feed)
        for i in range(1):
            assert Ls.created_post_tag[0].text == tag, f"Пост с тэгом - {tag} не найден"

    @allure.step("Проверить создался ли пост с картинкой")
    def check_picture_in_post(self, count_pic):
        Bs().wait_until_find(Ls.feed)
        assert count_pic[0].is_enabled(), f'К посту не добавлена картинка'

    @allure.step("Проверить количество картинок в посту")
    def check_invalid_count_picture_in_post(self, count_pic):
        Bs().wait_until_find(Ls.pictures_in_creating_post)
        assert len(Ls.pictures_in_creating_post) < count_pic, \
            f'К посту добавлено некорректное количество картинок - {len(Ls.pictures_in_creating_post)}'

    @allure.step("Проверить возможно ли создать пост - активна ли кнопка Опубликовать")
    def check_active_button_published(self):
        Bs().wait_until_find(Ls.feed)
        assert Ls.not_active_published_button.is_enabled(), \
            f'Кнопка опубликовать активна, это НЕ верное поведение в данном тесте'

    @allure.step("Проверить прикрепился ли пост")
    def check_fix_post(self):
        Bs().wait_until_find(Ls.post_is_fixed_article)
        assert Ls.post_is_fixed_article.is_displayed() and Ls.posts_fixed_title.is_displayed(), f'Пост не закреплен'

    @allure.step("Проверить открепился ли пост")
    def check_no_fix_post(self):
        Bs().wait_until_find(Ls.post_is_fixed_article)
        assert not (Ls.post_is_fixed_article.is_displayed() and Ls.posts_fixed_title.is_displayed()), f'Пост не ' \
                                                                                                      f'откреплен '

    @allure.step("Прикреплен ли первый пост")
    def check_first_post_fix(self, name):
        Bs().wait_until_find(Ls.post_is_fixed_article)
        assert Ls.texts_in_posts[0].text == name, 'Прикреплен не первый пост'

    @allure.step("Создался ли комментарий")
    def check_comment_in_post(self, comment):
        Bs().wait_until_find(Ls.first_post_comments_list)
        assert Ls.first_post_comments_list[0].text == comment, 'Комментарий НЕ создан'

    @allure.step("Удалился ли комментарий")
    def check_delete_comment_in_post(self):
        Bs().wait_until_find(Ls.feed)
        assert not Ls.first_post_comments_list[0].is_enabled(), \
            f'Комментарий - {Ls.first_post_comments_list[0].text} не удален'

    @allure.step("Активна ли кнопка удалить у чужого комментария")
    def check_active_button_on_over_comment_in_post(self):
        Bs().wait_until_find(Ls.first_post_comments_list)
        assert not Ls.delete_comment_button.is_enabled(), \
            f'Кнопка удалить у чужого комментария активна, возможно удаление чужого коммента'

    @allure.step("Проверить количество комментариев")
    def check_count_comment_in_post(self, count):
        Bs().wait_until_find(Ls.first_post_comments_list)
        assert len(Ls.first_post_comments_list) == count, \
            f'Количество видимых комментариев - {len(Ls.first_post_comments_list)}, а должно быть - {count}'

    @allure.step("Создались ли комментарии")
    def check_many_comments_in_post(self, comments):
        Bs().wait_until_find(Ls.first_post_comments_list)
        count = 0
        for i in range(len(Ls.first_post_comments_list)):
            if Ls.first_post_comments_list[i].text == comments[i]:
                count += 1
        assert count == len(Ls.first_post_comments_list),\
            f'Комментарии в количестве - {len(Ls.first_post_comments_list)} НЕ созданы'

    @allure.step("Проверить количество лайков")
    def check_count_likes_in_post(self, count):
        Bs().wait_until_find(Ls.first_post_count_of_likes)
        new_count = int(count) + 1
        assert int(Ls.first_post_count_of_likes.text) == new_count, \
            f'Количество лайков - {int(Ls.first_post_count_of_likes.text)}, а должно быть - {new_count}'

    @allure.step("Проверить количество просмотров")
    def check_count_views_in_post(self, count):
        Bs().wait_until_find(Ls.first_post_count_of_views)
        new_count = int(count) + 1
        assert int(Ls.first_post_count_of_views.text) == new_count, \
            f'Количество просмотров - {int(Ls.first_post_count_of_views.text)}, а должно быть - {new_count}'

    @allure.step("Проверить количество постов на странице при скролле вниз")
    def check_count_post_scroll_down(self, count):
        Bs().wait_until_find(Ls.texts_in_posts)
        assert len(Ls.post_text) > count, \
            f'Количество постов после скролла - {len(Ls.post_text)}, а должно быть больше чем- {count}'

    @allure.step("Проверить активна ли кнопка вверх на странице при скролле вверх")
    def check_button_up_when_scroll_up(self):
        Bs().wait_until_find(Ls.texts_in_posts)
        assert not Ls.button_up_is_active.is_enabled(), \
            f'Кнопка Вверх активна, значит не было осуществлен скролл вверх'

    @allure.step("Добавился ли вопрос и ответы к опросу, максимально 7 ответов")
    def check_question_answers_in_vote(self, question, count_answers):
        Bs().wait_until_find(Ls.texts_in_posts)
        assert Ls.vote_title.text == question, f'Опрос с вопросом {question} не создан'
        for i in range(count_answers):
            assert Ls.answers_for_vote[i] == Ls.created_vote_answers[i].text, \
                f' В опросе нет ответа -  {Ls.answers_for_vote[i]}'
