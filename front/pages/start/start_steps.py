from front.pages.lk.lk_page import LocatorsLk as Llk
from front.src.helpers.BaseSteps import BaseSteps as Bs
from front.pages.start.start_page import LocatorsStart as Ls
from front.pages.admin.admin_steps import AdminSteps as As
from picture_list import url_many_picture_jpg as pictures
from selene.api import *
import autoit
import allure


class StartSteps:
    @allure.step("Получить Фамилию авторизованного сотрудника'")
    def get_last_name_main(self):
        employee = Ls.fio_main.text.split(' ')
        return employee[0]

    @allure.step("Получить Имя авторизованного сотрудника'")
    def get_name_main(self):
        employee = Ls.fio_main.text.split(' ')
        return employee[1]

    @allure.step("Получить Имя авторизованного сотрудника'")
    def get_full_name_main(self):
        employee = Ls.fio_main.text
        return employee

    @allure.step("Добавить картинку к посту")
    def put_picture_post(self, picture):
        """Подключение библиотеки для работы с модальными окнами в Windows
        в функции передаем по порядку следования - имя окна, для ввода в поле
         "Имя файла" добавить - "Edit1", и далее то что вводим - сначала адрес,
          а потом кнопка интер"""
        Bs().wait_until_find(autoit.win_active(Llk.name_of_windows_download))
        autoit.win_active(Llk.name_of_windows_download)
        autoit.control_send(Llk.name_of_windows_download, "Edit1", f"{picture}")
        autoit.control_send(Llk.name_of_windows_download, "Edit1", "{ENTER}")
        autoit.control_send(Llk.name_of_windows_download, "Edit1", "{ENTER}")
        Bs().wait_until_find(Ls.feed)

    @allure.step("Добавить n картинок")
    def add_pictures(self, n):
        for i in range(n):
            Ls.add_picture_button.click()
            StartSteps().put_picture_post(pictures[i])

    @allure.step("Добавить n комментариев")
    def add_comments(self, n):
        comments_list = []
        Ls.post_add_comment_button[0].click()
        for i in range(n):
            Bs().wait_until_find(Ls.first_post_comments_list)
            comment = As().string_high_low_num_many(30) + Ls.spec_symbols_list + 'Комментарий №' + str(i+1)
            Ls.comment_input_field.send_keys(comment)
            Ls.add_comment_button.click()
            comments_list.append(comment)
        return comments_list

    @allure.step("Добавить n ответов к опросу, но не более 7")
    def add_answers(self, n):
        for i in range(n):
            """Вставляем сюда переменную поля ввода в start_page это vote_answers_field"""
            s(by.id(i)).send_keys(Ls.answers_for_vote[i])
