from selene.api import *
import string
import random


class LocatorsStartBirthdays:
    birthdays_title = s(by.xpath('//h3'))
    birthdays = s(by.xpath('//div/aside/div[1]/div/div/div/div'))
    birthdays_date = ss(by.xpath('//div/aside/div[1]/div/div//div/div/div[2]/div'))
    birthdays_button_close = s(by.xpath('//aside/div[1]/div/div/button'))
    birthday_button_open = s(by.xpath('//aside/div[1]/div/div/button'))
    birthdays_ava = ss(by.xpath('//aside/div[1]/div/div/div/div/div/a'))
    birthdays_fio = ss(by.xpath('//aside/div[1]/div//div/div/div/div/div/a'))


class LocatorsStart:
    start_title = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Scroll | Irlix")]]'))
    feed = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Лента")]]'))
    title_text = "Scroll | Irlix"
    creators_page = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Создатели")]]'))
    button_up = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Наверх")]]'))
    left_info_block = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Лента")]]'))
    right_info_block = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Дни рождения")]]'))
    ava_main = s(by.xpath('//header/div/div/div[1]/div[2]/div/a'))
    fio_main = s(by.xpath('//header/div/div/div[1]/div[2]/div/div/div[1]/a'))
    employees_page = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Сотрудники")]]'))
    geolocation_page = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "География")]]'))
    settings_page = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Настройки")]]'))
    welcome_page = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Welcome")]]'))
    admin_page = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Администрирование")]]'))
    wiki_page = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Wiki")]]'))
    apps_page = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Приложения")]]'))
    send_your_mind_page = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Оставить пожелания")]]'))
    exit_page = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Выйти")]]'))
    search_field = s(by.class_name('i-search__search-input'))
    search_field_x_button = s(by.class_name('i-search__cancel-btn'))
    received_data_search_field = s(by.class_name('i-search__results-content'))  # Далее будет сокращение rsdsf
    rsdsf_employees = s(by.xpath("//a[normalize-space(.)='Сотрудники']"))
    rsdsf_first_employee = s(by.xpath('//div[2]/div/nav[1]/a[1]'))
    rsdsf_posts = s(by.xpath("//a[normalize-space(.)='Посты']"))
    """В связи с тем что не возможно взять правильный xpath для первого элемента в списке постов, вводится два 
    локатора first_post, первый - для ввода символов на латинице, символов и чисел, второй - для ввода кириллицы"""
    rsdsf_first_post_eng_num_sym = s(by.xpath('//div[2]/div/nav[1]/a[1]'))
    rsdsf_first_post_rus = s(by.xpath('//nav[2]/a[1]'))
    post_text = ss(by.xpath('//p'))
    eng_key = 'a'
    rus_key = 'г'
    num_key = '1'
    spec_key = '@'
    rsdsf_first_tag = s(by.class_name('i-search__section-item'))
    tag = '#QA'
    tag_in_post_global_search = ss(by.class_name('i-post-content__tag active'))
    elements_in_global_search = ss(by.class_name('i-search__section-item'))
    rsdsf_first_element = s(by.xpath('//div[2]/div/nav[1]/a[1]'))
    post_page_title = s(by.class_name('i-search-page__title'))
    main_class_for_waits = s(by.class_name('i-main-page'))
    created_post_cross_line_text = s(by.xpath('//article[1]/div/div[2]/div/p/s'))
    created_post_fat_text = s(by.xpath('//article[1]/div/div[2]/div/p/strong'))
    created_post_link_text = s(by.xpath('//article[1]/div/div[2]/div/p/a'))
    created_post_tag = ss(by.class_name('i-post-content__tag'))
    cross_line_text_button = s(by.class_name('ql-strike'))
    fat_text_button = s(by.class_name('ql-bold'))
    list_text_button = s(by.class_name('ql-list'))
    clean_text_mod_button = s(by.class_name('ql-clean'))
    link_text_mod_button = s(by.class_name('ql-link'))
    link_text_mod_button_save = s(by.class_name('ql-action'))
    link_text_mod_input_field = s(by.xpath('//div[3]/input'))
    published_button = s(by.class_name("i-new-post-form__submit-btn"))
    not_active_published_button = s(by.class_name("i-new-post-form__submit-btn--disabled"))
    add_picture_button = s(by.class_name("i-new-post-form__add-photo-button"))
    open_tags_button = s(by.xpath("//div[2]/div/div[1]/span"))
    first_tag_in_list = s(by.xpath('//li[2]/span'))
    field_text_in_post = s(by.xpath('//div[1]/div/div[2]/div[1]/p'))
    calendar_post = s(by.class_name('i-dropdownPublication__button'))
    calendar_ok_button = s(by.class_name('ant-btn ant-btn-primary ant-btn-sm'))
    calendar_exit_button = s(by.class_name('ant-picker-clear'))
    go_next_year_calendar = s(by.class_name('ant-picker-header-super-next-btn'))
    go_next_mouth_calendar = s(by.class_name('ant-picker-header-next-btn'))
    vote_button = s(by.class_name('i-vote__button'))
    mark_text_in_create_post = s(by.xpath('//div[1]/div/div[2]/div[1]/p'))
    texts_in_first_post = s(by.xpath('//div[1]/p/a'))
    texts_in_posts = ss(by.class_name('i-post-content__txt-content'))
    ya_title_for_check = s(by.xpath('//main/div[1]/a'))
    url_ya_for_check = 'https://ya.ru/'
    first_post_block = ss(by.class_name('i-post__content'))
    post_modal_delete = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Удалить")]]'))
    post_modal_edit = s(by.xpath('//li[2]/span'))
    post_modal_share = s(by.xpath('//div[2]/div/div/ul/li/span'))
    first_post_edit_text_field = s(by.xpath('//div[2]/div[1]/div/div[2]/div[1]/p'))
    first_post_edit_text_field_button_ok = s(by.xpath('//div[2]/div/div/button[2]'))
    post_modal_to_fix = s(by.xpath('//li[1]/span'))
    posts_fixed_title = s(by.text('Закрепленные посты'))
    post_is_fixed_article = s(by.text('Пост закреплен'))
    post_open_modal = ss(by.class_name('i-post__select-button-container'))
    post_modal_button_yea_for_del = s(by.xpath('//div[2]/div/div/button[1]'))
    post_modal_button_no_for_del = s(by.xpath('//div[2]/div/div/button[2]'))
    post_modal_button_yea_for_fix = s(by.xpath('//*[@id="i-modal"]/div/div/button[1]'))
    post_modal_button_no_for_fix = s(by.xpath('//*[@id="i-modal"]/div/div/button[2]'))
    name_of_post = 'Уникальный новый пост - '+''.join(random.choices(string.digits, k=5))
    count_pictures_in_first_post = ss(by.xpath('//article[1]/div/div[2]/div[2]/img'))
    pictures_in_creating_post = ss(by.class_name('i-img-block__img-photo'))
    image_block_in_post_delete_button = ss(by.class_name('i-img-block__delete-div'))
    first_post_comments_list = ss(by.xpath("//article[1]/div/div[4]/div/div/div/div"))
    comment_input_field = s(by.class_name('i-add-comment__input'))
    post_add_comment_button = ss(by.class_name
                                 ("i-feedback-buttons__numbered-btn--type_comment"))
    add_comment_button = s(by.class_name('i-add-comment__send-button'))
    delete_comment_button = s(by.class_name('i-delete-button'))
    restore_comment_button = s(by.class_name('i-commentator__comment-restore-button'))
    close_comments_list_button = s(by.class_name('i-comments-list__hide-comments-button'))
    open_comments_list_button = s(by.class_name('i-comments-list__show-comments-button'))
    first_post_add_like_button = ss(by.class_name('i-feedback-buttons__numbered-btn--type_like'))[0]
    first_post_count_of_likes = ss(by.class_name('i-feedback-buttons__count'))[0]
    first_post_count_of_views = ss(by.class_name('i-feedback-buttons__count'))[2]
    spec_symbols_list = '=!/(),;:"@#$%^&*()_-+=/?'
    all_posts_title = s(by.class_name('i-tabs__item--active'))
    button_up_is_active = s(by.class_name('i-scroll--active'))
    vote_question_fild = s(by.xpath('//*[@id="i-modal"]/div/div/input'))
    question_for_vote = 'Играл в цитадель Псина?'
    vote_answers_field = s(by.id(0))
    answers_for_vote = ['Да, я шалун', 'Нет', 'А, что?', 'Ты что! ПЁС!',
                        'За Жириновского', 'Милорд, к нам приходят новые люди', 'Казна пуста, милорд']
    vote_title = s(by.class_name('i-vote__title'))
    created_vote_answers = ss(by.class_name('i-vote-answers__text'))
    create_vote_button = s(by.class_name('i-new-vote-form__submit-btn'))
