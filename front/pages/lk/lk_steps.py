from front.pages.lk.lk_page import LocatorsLk as Llk
from front.src.helpers.BaseSteps import BaseSteps as Bs
from front.pages.start.start_page import LocatorsStart as Ls
import autoit
import allure


class LkSteps:
    @allure.step("Добавить картинку на фон")
    def put_picture_background(self, picture):
        """Подключение библиотеки для работы с модальными окнами в Windows
        в функции передаем по порядку следования - имя окна, для ввода в поле
         "Имя файла" добавить - "Edit1", и далее то что вводим - сначала адрес,
          а потом кнопка интер"""
        Bs().wait_until_find(autoit.win_active(Llk.name_of_windows_download))
        autoit.win_active(Llk.name_of_windows_download)
        autoit.control_send(Llk.name_of_windows_download, "Edit1", f"{picture}")
        autoit.control_send(Llk.name_of_windows_download, "Edit1", "{ENTER}")
        autoit.control_send(Llk.name_of_windows_download, "Edit1", "{ENTER}")
        Bs().wait_until_find(Ls.fio_main)
        Ls.fio_main.click()

    @allure.step("Добавить валидную картинку на аву в лк")
    def put_picture_big_ava(self, picture):
        """Подключение библиотеки для работы с модальными окнами в Windows
        в функции передаем по порядку следования - имя окна, для ввода в поле
         "Имя файла" добавить - "Edit1", и далее то что вводим - сначала адрес,
          а потом кнопка интер"""
        Bs().wait_until_find(autoit.win_active(Llk.name_of_windows_download))
        autoit.win_active(Llk.name_of_windows_download)
        autoit.control_send(Llk.name_of_windows_download, "Edit1", f"{picture}")
        autoit.control_send(Llk.name_of_windows_download, "Edit2", "{ENTER}")
        Bs().wait_until_find(Ls.main_class_for_waits)
        if Llk.title_save_ava_window.is_enabled():
            Llk.modal_button_change_or_yes.click()
            Ls.fio_main.click()
        else:
            Ls.fio_main.click()

    @allure.step("Получить данные по картинке которая была до добавления новой")
    def get_old_picture(self, picture):
        Bs().wait_until_find(Llk.user_background)
        picture_fon = picture.get_attribute('style')
        if picture_fon == '':
            picture_ava = picture.get_attribute('src')
            return picture_ava
        return picture_fon

    @allure.step("Получить данные по добавленной картинке")
    def get_new_picture(self, picture):
        Bs().wait_until_find(Llk.user_background)
        picture_fon = picture.get_attribute('style')
        if picture_fon == '':
            picture_ava = picture.get_attribute('src')
            return picture_ava
        return picture_fon
