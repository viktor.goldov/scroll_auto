import allure
from front.src.helpers.BaseSteps import BaseSteps as Bs
from front.pages.lk.lk_page import LocatorsLk as Llk


class LkChecks:
    @allure.step("Проверить что картинка добавилась")
    def check_picture_is_set(self, old_picture, new_picture):
        assert old_picture != new_picture, 'Картинка не загрузилась'

    @allure.step("Проверить что картинка не добавилась (большая или не верного формата)")
    def check_picture_not_set(self, old_picture, new_picture):
        assert old_picture == new_picture, 'Картинка загрузилась'

    @allure.step("Проверить что картинка удалилась")
    def check_picture_is_del(self, old_picture, new_picture):
        assert old_picture != new_picture, 'Картинка не удалилась'

    @allure.step("Проверить что присутствует кнопка загрузки или удаления картинки")
    def check_else_picture_del_upload(self, button):
        Bs().wait_until_find(Llk.name)
        assert not button.is_enabled(), 'Присутствует возможность загружать или удалять картинку'

    @allure.step("Переход по ссылке в телеграмм")
    def check_go_telegram(self):
        Bs().first_tab()
        Bs().wait_until_find(Llk.name)
        user = Llk.telegram.text
        Bs().next_tab()
        telegram = Llk.telegram_page_nik.text
        assert user == telegram, 'Переход не был осуществлен'
