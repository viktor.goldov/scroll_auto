from selene.api import *


class LocatorsLk:
    name = s(by.class_name('i-user-page__user-full-name'))
    email = s(by.xpath('//div[4]/div[2]/div[1]/a'))
    check = s(by.xpath('//*[@id="root"]/div/main/div/div[4]/div[1]/div[1]/span'))
    geo = s(by.xpath('//div[4]/div[1]/div[1]/span'))
    department = s(by.xpath('//div[3]/div[2]/a'))
    user_background = s(by.class_name('i-user-page__background-block'))
    user_big_ava_lk = s(by.xpath('//div[2]/img'))
    user_background_upload = s(by.class_name('i-user-page__background-upload'))
    modal_button_change_or_yes = s(by.class_name('i-button'))
    modal_button_delete_or_cancel = s(by.xpath('//*[@id="i-modal"]/div/div/button'))
    user_photo_delete_button = s(by.class_name('i-user-page__user-photo-delete'))
    user_photo_update_button = s(by.class_name('i-user-page__user-photo-update'))
    url_picture_jpg = 'https://freelance.today/uploads/images/00/07/62/2017/06/13/14c404.jpg'
    url_picture_png = 'https://overcoder.net/img/1/2/99/19825.png'
    url_picture_webp = 'https://www.gstatic.com/webp/gallery/4.webp'
    url_picture_invalid = 'https://thumbs.gfycat.com/GenerousColossalCurlew-size_restricted.gif'
    url_picture_big = 'https://thumb.cloud.mail.ru/weblink/thumb/xw1/FxN1/Tu3zDKZhh'
    url_invalid_file = 'http://www.anovikov.ru/books/anec.pdf'
    name_of_windows_download = "Открытие"
    telegram = s(by.xpath('//div[3]/a'))
    telegram_page_nik = s(by.class_name('tgme_page_extra'))
    title_save_ava_window = s(by.class_name('i-modal__title'))
