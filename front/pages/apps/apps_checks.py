from front.pages.apps.apps_page import LocatorsApps as Lapp

import allure


class ChecksApp:

    @allure.step("Проверить что количество игр соответствует требованиям")
    def check_count_games(self, count_tz):
        assert count_tz == len(Lapp.all_games), f'Количество игр равно {len(Lapp.all_games)},' \
                                                f' что не соответствует требованиям'
