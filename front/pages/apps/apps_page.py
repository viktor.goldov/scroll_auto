from selene.api import *


class LocatorsApps:
    title_apps = s(by.class_name('i-apps-page__title'))
    booking_open_button = s(by.xpath("//article[1]/div/a"))
    value_open_button = s(by.xpath("//article[2]/div/a"))
    booking_page_title = s(by.class_name('i-header__logo'))
    value_page_title = s(by.class_name('header__name'))
    service_button = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Сервисы")]]'))
    game_button = s(by.xpath('.//*[text()[normalize-space(.) = concat("", "Игры")]]'))
    all_games = ss(by.class_name('i-app-card__name'))
