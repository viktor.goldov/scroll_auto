from front.pages.employees.employees_page import LocatorsEmp as Lem
import allure


class EmployeesSteps:
    @allure.step("Ввести в поле поиска и нажать Enter")
    def put_in_field(self, element):
        Lem.find_field.send_keys(element).press_enter()

    @allure.step("Очистить поле поиска'")
    def clear_field(self):
        Lem.close_find_field.click()

    @allure.step("Получить список всех сотрудников - фамилия, имя")
    def get_employee_list(self):
        list_employees = []
        for i in range(len(Lem.fio_employees())):
            list_employees.append(Lem.fio_employees()[i])
        return list_employees
