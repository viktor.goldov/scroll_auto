import allure
from front.src.helpers.BaseSteps import BaseSteps as Bs
from front.pages.lk.lk_page import LocatorsLk as Lk
from front.pages.employees.employees_page import LocatorsEmp as Lem
from front.pages.employees.employees_steps import EmployeesSteps as Emps
from time import sleep
from selenium.webdriver.common.keys import Keys


class EmployeesChecks:
    @allure.step("Проверить сотрудников согласно выбранному направлению в фильтре")
    def check_department_filter(self):
        Bs().wait_until_find(Lem.department_list)
        departs = Lem.department_list
        for i in range(1, len(departs)):
            Bs().wait_until_find(departs[i].text)
            depart = departs[i].text
            departs[i].click()
            Bs().wait_until_find(Lem.department_user)
            depart_user = Lem.department_user
            for j in range(len(depart_user)):
                a = depart_user[j].text.lower()
                assert depart.lower() == a, f'Фильтр по направлению -  {depart},' \
                                            f' а у пользователя направление -  {depart_user[j].text}'
        departs[0].click()

    @allure.step("Проверить сотрудников согласно выбранному городу в фильтре")
    def check_cities_filter(self):
        Bs().wait_until_find(Lem.cities_list)
        cities = Lem.cities_list
        """Проверить первые 7 городов, если нужно проверить всё, то вставить - range(1,len(cities))"""
        for i in range(1, 8):
            city = cities[i].text
            cities[i].click()
            Bs().wait_until_find(Lem.fio_employees)
            fio_s = Lem.fio_employees
            for j in range(len(fio_s)):
                """Функция для перехода в ЛК"""
                fio_s[j].send_keys((Keys.CONTROL + Keys.ENTER))
                Bs().next_tab()
                """Подождать пока найдётся элемент"""
                Bs().wait_until_find(Lk.geo)
                city_user = Lk.geo.text
                assert city == city_user, f'Фильтр по городу - {city}, а у пользователя город -  {city_user}'
                Bs().close_tab()
                Bs().first_tab()

    @allure.step("Проверить сотрудников согласно выбранному городу и направлению в фильтре")
    def check_cities_department_filter(self):
        Bs().wait_until_find(Lem.cities_list)
        cities = Lem.cities_list
        departs = Lem.department_list
        """Проверить первые 7 городов, если нужно проверить всё, то вставить - range(1, len(cities)"""
        for i in range(1, 8):
            city = cities[i].text
            cities[i].click()
            for k in range(1, len(departs)):
                depart = departs[k].text
                departs[k].click()
                Bs().wait_until_find(Lem.department_user)
                fio_s = Lem.fio_employees
                for j in range(len(fio_s)):
                    """Функция для перехода в ЛК"""
                    fio_s[j].send_keys((Keys.CONTROL + Keys.ENTER))
                    Bs().next_tab()
                    """Подождать пока найдётся элемент"""
                    Bs().wait_until_find(Lk.geo)
                    """Подождать пока найдётся элемент"""
                    Bs().wait_until_find(Lk.department)
                    depart_user = Lk.department.text
                    city_user = Lk.geo.text
                    assert city == city_user and depart == depart_user, \
                        f'Фильтр по городу - {city} и направлению {depart}, а у пользователя город -  {city_user}' \
                        f' и направление -  {depart_user}'
                    Bs().close_tab()
                    Bs().first_tab()

    @allure.step("Проверить сотрудников согласно фамилии")
    def check_by_last_name(self):
        Bs().wait_until_find(Lem.fio_employees)
        employees = Lem.fio_employees
        """Проверить первых 7 сотрудников, если нужно проверить всех, то вставить - range(len(employees))"""
        for i in range(7):
            names = employees[i]
            last_name = names.text.split(' ')[0]
            Emps().put_in_field(last_name)
            Bs().wait_until_find(Lem.fio_employees)
            fio_s = Lem.fio_employees
            for j in range(len(fio_s)):
                list_1 = list(last_name)
                list_2 = list(fio_s[j].text.split(' ')[0])
                for k in range(len(list_1)):
                    assert list_1[k] == list_2[k], f"Фильтр по фамилии- {last_name}," \
                                                   f" а в списке присутствует фамилия - {fio_s[j].text.split(' ')[0]}"
            Emps().clear_field()

    @allure.step("Проверить сотрудников согласно имени")
    def check_by_first_name(self):
        Bs().wait_until_find(Lem.fio_employees)
        employees = Lem.fio_employees
        names_list = []
        """Проверить первых 7 сотрудников, если нужно проверить всех, то вставить - range(len(employees))"""
        for i in range(7):
            Bs().wait_until_find(Lem.fio_employees)
            names = employees[i]
            name = names.text.split(' ')[1]
            if name in names_list:
                continue
            else:
                Emps().put_in_field(name)
                Bs().wait_until_find(Lem.fio_employees)
                fio_s = Lem.fio_employees
                for j in range(len(fio_s)):
                    list_1 = list(name)
                    list_2 = list(fio_s[j].text.split(' ')[1])
                    """Если фамилия содержит часть имени, используйте list_3"""
                    list_3 = list(fio_s[j].text.split(' ')[0])
                    """"Ниже приведена побуквенная проверка каждого имени."""
                    for k in range(len(list_1)):
                        if len(list_1) <= len(list_2) and len(list_1) <= len(list_3):
                            a = list_2[:len(list_1)][k]
                            b = list_3[:len(list_1)][k]
                            """Ниже добавлена проверка, если мы встретили Ё, то пропускаем эту букву"""
                            if list_1[k] == 'ё' or a == 'ё' \
                                    or b == 'ё':
                                continue
                            else:
                                assert list_1[k] == list_2[:len(list_1)][k] or list_1[k] == list_3[:len(list_1)][k], \
                                    f"Выбран фильтр по имени - {name}," \
                                    f"а в списке присутствует имя - {fio_s[j].text.split(' ')[1]}"
                        elif len(list_2) < len(list_1) <= len(list_3):
                            b = list_3[:len(list_1)][k]
                            if list_1[k] == 'ё' or b == 'ё':
                                continue
                            else:
                                assert list_1[k] == list_3[:len(list_1)][k], \
                                    f"Выбран фильтр по имени - {name}," \
                                    f"а в списке присутствует имя - {fio_s[j].text.split(' ')[1]}"
                        else:
                            a = list_2[:len(list_1)][k]
                            if list_1[k] == 'ё' or a == 'ё':
                                continue
                            else:
                                assert list_1[k] == list_2[:len(list_1)][k], \
                                    f"Выбран фильтр по имени - {name}," \
                                    f"а в списке присутствует имя - {fio_s[j].text.split(' ')[1]}"
                    names_list.append(name)
                Emps().clear_field()

    @allure.step("Проверить сотрудников согласно фамилии + пробелы в начале")
    def check_by_last_name_spaces(self):
        Bs().wait_until_find(Lem.fio_employees)
        employees = Lem.fio_employees
        """Проверить первых 7 сотрудников, если нужно проверить всех, то вставить - range(len(employees))"""
        for i in range(7):
            names = employees[i]
            last_name = names.text.split(' ')[0]
            Emps().put_in_field("     " + last_name)
            Bs().wait_until_find(Lem.fio_employees)
            fio_s = Lem.fio_employees
            for j in range(len(fio_s)):
                list_1 = list(last_name)
                list_2 = list(fio_s[j].text.split(' ')[0])
                for k in range(len(list_1)):
                    assert list_1[k] == list_2[k], f"Фильтр по фамилии- {last_name}," \
                                                   f" а в списке присутствует фамилия - {fio_s[j].text.split(' ')[0]}"
            Emps().clear_field()

    @allure.step("Проверить сотрудников согласно имени + пробелы в начале")
    def check_by_first_name_spaces(self):
        Bs().wait_until_find(Lem.fio_employees)
        employees = Lem.fio_employees
        names_list = []
        """Проверить первых 7 сотрудников, если нужно проверить всех, то вставить - range(len(employees))"""
        for i in range(7):
            Bs().wait_until_find(Lem.fio_employees)
            names = employees[i]
            name = names.text.split(' ')[1]
            if name in names_list:
                continue
            else:
                Emps().put_in_field('     ' + name)
                Bs().wait_until_find(Lem.fio_employees)
                fio_s = Lem.fio_employees
                for j in range(len(fio_s)):
                    list_1 = list(name)
                    list_2 = list(fio_s[j].text.split(' ')[1])
                    """Если фамилия содержит часть имени, используйте list_3"""
                    list_3 = list(fio_s[j].text.split(' ')[0])
                    """"Ниже приведена побуквенная проверка каждого имени."""
                    for k in range(len(list_1)):
                        if len(list_1) <= len(list_2) and len(list_1) <= len(list_3):
                            a = list_2[:len(list_1)][k]
                            b = list_3[:len(list_1)][k]
                            """Ниже добавлена проверка, если мы встретили Ё, то пропускаем эту букву"""
                            if list_1[k] == 'ё' or a == 'ё' \
                                    or b == 'ё':
                                continue
                            else:
                                assert list_1[k] == list_2[:len(list_1)][k] or list_1[k] == list_3[:len(list_1)][k], \
                                    f"Выбран фильтр по имени - {name}," \
                                    f"а в списке присутствует имя - {fio_s[j].text.split(' ')[1]}"
                        elif len(list_2) < len(list_1) <= len(list_3):
                            b = list_3[:len(list_1)][k]
                            if list_1[k] == 'ё' or b == 'ё':
                                continue
                            else:
                                assert list_1[k] == list_3[:len(list_1)][k], \
                                    f"Выбран фильтр по имени - {name}," \
                                    f"а в списке присутствует имя - {fio_s[j].text.split(' ')[1]}"
                        else:
                            a = list_2[:len(list_1)][k]
                            if list_1[k] == 'ё' or a == 'ё':
                                continue
                            else:
                                assert list_1[k] == list_2[:len(list_1)][k], \
                                    f"Выбран фильтр по имени - {name}," \
                                    f"а в списке присутствует имя - {fio_s[j].text.split(' ')[1]}"
                    names_list.append(name)
                Emps().clear_field()

    @allure.step("Проверить сотрудников согласно имени и городу")
    def check_by_first_name_city_filter(self):
        Bs().wait_until_find(Lem.fio_employees)
        employees = Lem.fio_employees
        names_list = []
        cities = Lem.cities_list
        """Проверить первых 7 сотрудников, если нужно проверить всех, то вставить - range(len(employees))"""
        for i in range(7):
            Bs().wait_until_find(Lem.fio_employees)
            names = employees[i]
            name = names.text.split(' ')[1]
            if name in names_list:
                continue
            else:
                Emps().put_in_field(name)
                for e in range(1, len(cities)):
                    Bs().wait_until_find(cities[e].text)
                    city = cities[e].text
                    cities[e].click()
                    Bs().wait_until_find(Lem.fio_employees)
                    fio_s = Lem.fio_employees
                    """Включение в проверку фильтра - город"""
                    for f in range(len(fio_s)):
                        """Функция для перехода в ЛК"""
                        fio_s[f].send_keys((Keys.CONTROL + Keys.ENTER))
                        Bs().next_tab()
                        """Подождать пока найдётся элемент"""
                        Bs().wait_until_find(Lk.geo)
                        city_user = Lk.geo.text
                        assert city == city_user, f'Фильтр по городу - {city}, ' \
                                                  f'а у пользователя {fio_s[e]} город -  {city_user}'
                        Bs().close_tab()
                        Bs().first_tab()
                        """Включение в проверку фильтрации по имени"""
                        for j in range(len(fio_s)):
                            list_1 = list(name)
                            list_2 = list(fio_s[j].text.split(' ')[1])
                            """Если фамилия содержит часть имени, используйте list_3"""
                            list_3 = list(fio_s[j].text.split(' ')[0])
                            """"Ниже приведена побуквенная проверка каждого имени."""
                            for k in range(len(list_1)):
                                if len(list_1) <= len(list_2) and len(list_1) <= len(list_3):
                                    a = list_2[:len(list_1)][k]
                                    b = list_3[:len(list_1)][k]
                                    """Ниже добавлена проверка, если мы встретили Ё, то пропускаем эту букву"""
                                    if list_1[k] == 'ё' or a == 'ё' \
                                            or b == 'ё':
                                        continue
                                    else:
                                        assert list_1[k] == list_2[:len(list_1)][k] or list_1[k] == \
                                               list_3[:len(list_1)][k], f"Выбран фильтр по имени - {name}," \
                                                                        f" а в списке присутствует имя " \
                                                                        f"- {fio_s[j].text.split(' ')[1]}"
                                elif len(list_2) < len(list_1) <= len(list_3):
                                    b = list_3[:len(list_1)][k]
                                    if list_1[k] == 'ё' or b == 'ё':
                                        continue
                                    else:
                                        assert list_1[k] == list_3[:len(list_1)][k], \
                                            f"Выбран фильтр по имени - {name}," \
                                            f"а в списке присутствует имя - {fio_s[j].text.split(' ')[1]}"
                                else:
                                    a = list_2[:len(list_1)][k]
                                    if list_1[k] == 'ё' or a == 'ё':
                                        continue
                                    else:
                                        assert list_1[k] == list_2[:len(list_1)][k], \
                                            f"Выбран фильтр по имени - {name}," \
                                            f"а в списке присутствует имя - {fio_s[j].text.split(' ')[1]}"
                cities[0].click()
                names_list.append(name)
            Emps().clear_field()

    @allure.step("Проверить сотрудников согласно имени и направлению")
    def check_by_first_name_depart_filter(self):
        Bs().wait_until_find(Lem.fio_employees)
        employees = Lem.fio_employees
        names_list = []
        departs = Lem.department_list
        """Проверить первых 7 сотрудников, если нужно проверить всех, то вставить - range(len(employees))"""
        for i in range(7):
            names = employees[i]
            name = names.text.split(' ')[1]
            if name in names_list:
                continue
            else:
                Emps().put_in_field(name)
                Bs().wait_until_find(Lem.fio_employees)
                fio_s = Lem.fio_employees
                for e in range(1, len(departs)):
                    Bs().wait_until_find(departs[e].text)
                    depart = departs[e].text
                    departs[e].click()
                    Bs().wait_until_find(Lem.department_user)
                    depart_user = Lem.department_user
                    """Включение в проверку фильтра - направление"""
                    for f in range(len(depart_user)):
                        a = depart_user[f].text.lower()
                        assert depart.lower() == a, f'Фильтр по направлению -  {depart},' \
                                                    f' а у пользователя направление -  {depart_user[f].text}'
                        """Включение в проверку фильтрации по имени"""
                        for j in range(len(fio_s)):
                            list_1 = list(name)
                            list_2 = list(fio_s[j].text.split(' ')[1])
                            """Если фамилия содержит часть имени, используйте list_3"""
                            list_3 = list(fio_s[j].text.split(' ')[0])
                            """"Ниже приведена побуквенная проверка каждого имени."""
                            for k in range(len(list_1)):
                                if len(list_1) <= len(list_2) and len(list_1) <= len(list_3):
                                    a = list_2[:len(list_1)][k]
                                    b = list_3[:len(list_1)][k]
                                    """Ниже добавлена проверка, если мы встретили Ё, то пропускаем эту букву"""
                                    if list_1[k] == 'ё' or a == 'ё' \
                                            or b == 'ё':
                                        continue
                                    else:
                                        assert list_1[k] == list_2[:len(list_1)][k] or list_1[k] == \
                                               list_3[:len(list_1)][k], f"Выбран фильтр по имени - {name}," \
                                                                        f"а в списке присутствует имя " \
                                                                        f"- {fio_s[j].text.split(' ')[1]}"
                                elif len(list_2) < len(list_1) <= len(list_3):
                                    b = list_3[:len(list_1)][k]
                                    if list_1[k] == 'ё' or b == 'ё':
                                        continue
                                    else:
                                        assert list_1[k] == list_3[:len(list_1)][k], \
                                            f"Выбран фильтр по имени - {name}," \
                                            f"а в списке присутствует имя - {fio_s[j].text.split(' ')[1]}"
                                else:
                                    a = list_2[:len(list_1)][k]
                                    if list_1[k] == 'ё' or a == 'ё':
                                        continue
                                    else:
                                        assert list_1[k] == list_2[:len(list_1)][k], \
                                            f"Выбран фильтр по имени - {name}," \
                                            f"а в списке присутствует имя - {fio_s[j].text.split(' ')[1]}"
                departs[0].click()
                names_list.append(name)
            Emps().clear_field()

    @allure.step("Проверить сотрудников согласно фамилии и городу")
    def check_by_last_name_city_filter(self):
        Bs().wait_until_find(Lem.fio_employees)
        employees = Lem.fio_employees
        """Проверить первых 7 сотрудников, если нужно проверить всех, то вставить - range(len(employees))"""
        for i in range(7):
            names = employees[i]
            last_name = names.text.split(' ')[0]
            Emps().put_in_field(last_name)
            Bs().wait_until_find(Lem.cities_list)
            cities = Lem.cities_list
            """Проверить первые 7 городов, если нужно проверить всё, то вставить - range(1, len(cities))"""
            for k in range(1, 8):
                city = cities[k].text
                cities[k].click()
                Bs().wait_until_find(Lem.fio_employees)
                fio_s = Lem.fio_employees
                """Включение в проверку фильтра - город"""
                for e in range(len(fio_s)):
                    """Функция для перехода в ЛК"""
                    fio_s[e].send_keys((Keys.CONTROL + Keys.ENTER))
                    Bs().next_tab()
                    """Подождать пока найдётся элемент"""
                    Bs().wait_until_find(Lk.geo)
                    city_user = Lk.geo.text
                    assert city == city_user, f'Фильтр по городу - {city}, а у пользователя город -  {city_user}'
                    Bs().close_tab()
                    Bs().first_tab()
                    """Включение в проверку фильтрации по фамилии"""
                    for j in range(len(fio_s)):
                        list_1 = list(last_name)
                        list_2 = list(fio_s[j].text.split(' ')[0])
                        for s in range(len(list_1)):
                            assert list_1[s] == list_2[s], f"Фильтр по фамилии- {last_name}," \
                                                           f" а в списке присутствует фамилия -" \
                                                           f" {fio_s[s].text.split(' ')[0]}"
            cities[0].click()
            Emps().clear_field()

    @allure.step("Проверить сотрудников согласно фамилии и направлению")
    def check_by_last_name_depart_filter(self):
        Bs().wait_until_find(Lem.fio_employees)
        employees = Lem.fio_employees
        departs = Lem.department_list
        """Проверить первых 7 сотрудников, если нужно проверить всех, то вставить - range(len(employees))"""
        for i in range(7):
            names = employees[i]
            last_name = names.text.split(' ')[0]
            Emps().put_in_field(last_name)
            Bs().wait_until_find(Lem.fio_employees)
            fio_s = Lem.fio_employees
            for e in range(1, len(departs)):
                Bs().wait_until_find(departs[e].text)
                depart = departs[e].text
                departs[e].click()
                Bs().wait_until_find(Lem.department_user)
                depart_user = Lem.department_user
                """Включение в проверку фильтра - направление"""
                for f in range(len(depart_user)):
                    a = depart_user[f].text.lower()
                    assert depart.lower() == a, f'Фильтр по направлению -  {depart},' \
                                                f' а у пользователя направление -  {depart_user[f].text}'
                    """Включение в проверку фильтрации по фамилии"""
                    for j in range(len(fio_s)):
                        list_1 = list(last_name)
                        list_2 = list(fio_s[j].text.split(' ')[0])
                        for s in range(len(list_1)):
                            assert list_1[s] == list_2[s], f"Фильтр по фамилии- {last_name}," \
                                                           f" а в списке присутствует фамилия -" \
                                                           f" {fio_s[s].text.split(' ')[0]}"
            departs[0].click()
            Emps().clear_field()

    @allure.step("Проверить сотрудников согласно фамилии, направлению и городу")
    def check_by_last_name_city_depart_filter(self):
        Bs().wait_until_find(Lem.fio_employees)
        employees = Lem.fio_employees
        departs = Lem.department_list
        cities = Lem.cities_list
        """Проверить 6-го и 7-го сотрудника, если нужно проверить всех, то вставить - range(len(employees))"""
        for i in range(4, 5):
            names = employees[i]
            last_name = names.text.split(' ')[0]
            Emps().put_in_field(last_name)
            Bs().wait_until_find(Lem.cities_list)
            """Проверить первые 7 городов, если нужно проверить всё, то вставить - range(1, len(cities))"""
            for k in range(1, 8):
                city = cities[k].text
                cities[k].click()
                Bs().wait_until_find(departs)
                for q in range(1, len(departs)):
                    depart = departs[q].text
                    departs[q].click()
                    Bs().wait_until_find(Lem.department_user)
                    depart_user = Lem.department_user
                    fio_s = Lem.fio_employees
                    """Включение в проверку фильтра - направление"""
                    for f in range(len(depart_user)):
                        a = depart_user[f].text.lower()
                        assert depart.lower() == a, f'Фильтр по направлению -  {depart},' \
                                                    f' а у пользователя направление -  {depart_user[f].text}'
                        """Включение в проверку фильтра - город"""
                        for e in range(len(fio_s)):
                            """Функция для перехода в ЛК"""
                            fio_s[e].send_keys((Keys.CONTROL + Keys.ENTER))
                            Bs().next_tab()
                            """Подождать пока найдётся элемент"""
                            Bs().wait_until_find(Lk.geo)
                            city_user = Lk.geo.text
                            assert city == city_user, f'Фильтр по городу - {city}, ' \
                                                      f'а у пользователя город -  {city_user}'
                            Bs().close_tab()
                            Bs().first_tab()
                            """Включение в проверку фильтрации по фамилии"""
                            for j in range(len(fio_s)):
                                list_1 = list(last_name)
                                list_2 = list(fio_s[j].text.split(' ')[0])
                                for s in range(len(list_1)):
                                    assert list_1[s] == list_2[s], f"Фильтр по фамилии- {last_name}," \
                                                                   f" а в списке присутствует фамилия -" \
                                                                   f" {fio_s[s].text.split(' ')[0]}"
                cities[0].click()
                departs[0].click()
            Emps().clear_field()

    @allure.step("Проверить сотрудников согласно имени, города и направления")
    def check_by_first_name_city_depart_filter(self):
        Bs().wait_until_find(Lem.fio_employees)
        employees = Lem.fio_employees
        names_list = []
        departs = Lem.department_list
        cities = Lem.cities_list
        """Проверить 6-го и 7-го сотрудника, если нужно проверить всех, то вставить - range(len(employees))"""
        for i in range(5, 7):
            names = employees[i]
            name = names.text.split(' ')[1]
            if name in names_list:
                continue
            else:
                Emps().put_in_field(name)
                Bs().wait_until_find(departs)
                for e in range(1, len(departs)):
                    depart = departs[e].text
                    departs[e].click()
                    Bs().wait_until_find(Lem.department_user)
                    depart_user = Lem.department_user
                    """Проверить первые 7 городов, если нужно проверить всё, то вставить - range(1, len(cities))"""
                    for w in range(1, 8):
                        city = cities[w].text
                        cities[w].click()
                        Bs().wait_until_find(Lem.fio_employees)
                        fio_s = Lem.fio_employees
                        """Включение в проверку фильтра - город"""
                        for n in range(len(fio_s)):
                            """Функция для перехода в ЛК"""
                            fio_s[n].send_keys((Keys.CONTROL + Keys.ENTER))
                            Bs().next_tab()
                            """Подождать пока найдётся элемент"""
                            Bs().wait_until_find(Lk.geo)
                            city_user = Lk.geo.text
                            assert city == city_user, f'Фильтр по городу - {city},' \
                                                      f' а у пользователя город -  {city_user}'
                            Bs().close_tab()
                            Bs().first_tab()
                            """Включение в проверку фильтра - направление"""
                            for f in range(len(depart_user)):
                                assert depart.lower() == depart_user[f].text.lower(), \
                                    f'Фильтр по направлению -  {depart},' \
                                    f' а у пользователя направление -  {depart_user[f].text}'
                                """Включение в проверку фильтрации по имени"""
                                for j in range(len(fio_s)):
                                    list_1 = list(name)
                                    list_2 = list(fio_s[j].text.split(' ')[1])
                                    """Если фамилия содержит часть имени, используйте list_3"""
                                    list_3 = list(fio_s[j].text.split(' ')[0])
                                    """"Ниже приведена побуквенная проверка каждого имени."""
                                    for k in range(len(list_1)):
                                        if len(list_1) <= len(list_2) and len(list_1) <= len(list_3):
                                            a = list_2[:len(list_1)][k]
                                            b = list_3[:len(list_1)][k]
                                            """Ниже добавлена проверка, если мы встретили Ё, то пропускаем эту букву"""
                                            if list_1[k] == 'ё' or a == 'ё' \
                                                    or b == 'ё':
                                                continue
                                            else:
                                                assert list_1[k] == list_2[:len(list_1)][k] or list_1[k] == \
                                                       list_3[:len(list_1)][k], f"Выбран фильтр по имени - {name}," \
                                                                                f"а в списке присутствует имя " \
                                                                                f"- {fio_s[j].text.split(' ')[1]}"
                                        elif len(list_2) < len(list_1) <= len(list_3):
                                            b = list_3[:len(list_1)][k]
                                            if list_1[k] == 'ё' or b == 'ё':
                                                continue
                                            else:
                                                assert list_1[k] == list_3[:len(list_1)][k], \
                                                    f"Выбран фильтр по имени - {name}," \
                                                    f"а в списке присутствует имя - {fio_s[j].text.split(' ')[1]}"
                                        else:
                                            a = list_2[:len(list_1)][k]
                                            if list_1[k] == 'ё' or a == 'ё':
                                                continue
                                            else:
                                                assert list_1[k] == list_2[:len(list_1)][k], \
                                                    f"Выбран фильтр по имени - {name}," \
                                                    f"а в списке присутствует имя - {fio_s[j].text.split(' ')[1]}"
                cities[0].click()
                departs[0].click()
                names_list.append(name)
            Emps().clear_field()

    @allure.step("Проверить сотрудников вбив в поиск Фамилию и Имя")
    def check_by_lastname_name(self):
        Bs().wait_until_find(Lem.fio_employees)
        count = 0
        employees = Lem.fio_employees
        for i in range(len(employees)):
            sleep(1)
            names = employees[i]
            name = names.text
            Emps().put_in_field(name)
            Bs().wait_until_find(Lem.fio_employees)
            fio_s = Lem.fio_employees
            for j in range(len(fio_s)):
                assert name == fio_s[j].text, f"Выбран фильтр по имени - {name}," \
                                              f" а в списке присутствует имя - {fio_s[j].text}"
                count += 1
                """Если нужно посмотреть текущий номер каждого сотрудника то откройте строку ниже"""
                # print(name, '=', count)
            Emps().clear_field()
        sleep(2)
        assert f'Количество: {count}' == Lem.number_employees.text, \
            f'Ошибка в проверке сотрудников, проверенно {count}, должно быть {Lem.number_employees.text}'

    @allure.step("Проверить сотрудников вбив в поиск Имя и Фамилию")
    def check_by_name_lastname(self):
        Bs().wait_until_find(Lem.fio_employees)
        employees = Lem.fio_employees
        """Проверить первых 10 сотрудников, если нужно проверить всех, то вставить - range(len(employees))"""
        for i in range(10):
            Bs().wait_until_find(Lem.fio_employees)
            names = employees[i]
            original_name = names.text
            name_to_reverse = names.text.split(' ')
            name_to_reverse.reverse()
            reversed_name = " ".join(name_to_reverse)
            Emps().put_in_field(reversed_name)
            sleep(1)
            fio_s = Lem.fio_employees
            for j in range(len(fio_s)):
                assert original_name == fio_s[j].text, f"Выбран фильтр по имени - {name_to_reverse}," \
                                                       f" а в списке присутствует имя - {fio_s[j].text}"
            Emps().clear_field()

    @allure.step("Проверить сотрудников вбив в поиск Имя и Фамилию + пробелы до и после")
    def check_by_name_lastname_spaces(self):
        Bs().wait_until_find(Lem.fio_employees)
        employees = Lem.fio_employees
        """Проверить первых 10 сотрудников, если нужно проверить всех, то вставить - range(len(employees))"""
        for i in range(10):
            Bs().wait_until_find(Lem.fio_employees)
            names = employees[i]
            name = names.text
            name_spaces = '     ' + names.text + '     '
            Emps().put_in_field(name_spaces)
            Bs().wait_until_find(Lem.fio_employees)
            fio_s = Lem.fio_employees
            for j in range(len(fio_s)):
                assert name == fio_s[j].text, f"Выбран фильтр по имени - {name}," \
                                              f" а в списке присутствует имя - {fio_s[j].text}"
            Emps().clear_field()

    @allure.step("Проверить переход по клику на Аву сотрудника в лк юзера")
    def check_ava_go_lk(self):
        Bs().wait_until_find(Lem.fio_employees)
        avas = Lem.ava_employees
        name = Lem.fio_employees
        """Проверить первых 7 сотрудников, если нужно проверить всех, то вставить - range(len(avas))"""
        for i in range(7):
            user = avas[i].get_attribute('href').split('/')
            """Функция для перехода в ЛК"""
            avas[i].send_keys((Keys.CONTROL + Keys.ENTER))
            Bs().next_tab()
            """Подождать пока найдётся элемент"""
            Bs().wait_until_find(Lk.email)
            lk_user = Lk.email.text.split('@')
            """Меняем местами имя и фамилию для доп проверки, если в почте и логине поменяны они местами"""
            reverse_user = user[4].split('.')
            lk_user_for_reverse = lk_user[0].split('.')
            reverse_user.reverse()
            assert user[4] == lk_user[0] or reverse_user == lk_user_for_reverse, \
                f'Переход с экрана Сотрудники в личный кабинет пользователя {name[i].text} НЕ осуществлен'
            Bs().close_tab()
            Bs().first_tab()

    @allure.step("Проверить переход по клику на ФИ  сотрудника в лк юзера")
    def check_fi_go_lk(self):
        Bs().wait_until_find(Lem.fio_employees)
        fi = Lem.fio_employees
        """Проверить первых 7 сотрудников, если нужно проверить всех, то вставить - range(len(fi))"""
        for i in range(7):
            user = fi[i].get_attribute('href').split('/')
            """Функция для перехода в ЛК"""
            fi[i].send_keys((Keys.CONTROL + Keys.ENTER))
            Bs().next_tab()
            """Подождать пока найдётся элемент"""
            Bs().wait_until_find(Lk.email)
            lk_user = Lk.email.text.split('@')
            """Меняем местами имя и фамилию для доп проверки, если в почте и логине поменяны они местами"""
            reverse_user = user[4].split('.')
            lk_user_for_reverse = lk_user[0].split('.')
            reverse_user.reverse()
            assert user[4] == lk_user[0] or reverse_user == lk_user_for_reverse, \
                f'Переход с экрана Сотрудники в личный кабинет пользователя {fi[i].text} НЕ осуществлен'
            Bs().close_tab()
            Bs().first_tab()

    @allure.step("Проверить использование буквы 'Е' для поиска сотрудника с ФИ содержащие букву 'Ё'")
    def check_e_in_fullname(self):
        Bs().wait_until_find(Lem.fio_employees)
        employees = Lem.fio_employees
        for i in range(len(employees)):
            names = employees[i].text.split(' ')
            name = names[1]
            last_name = names[0]
            """Проверяем побуквенно есть ли в имени буква 'Ё', если есть то меняем её на 'Е' и вставляем в поиск"""
            for k in range(len(name)):
                if name[k].lower() == 'ё':
                    lis_name = list(name.lower())
                    index_e = lis_name.index('ё')
                    lis_name.remove('ё')
                    lis_name.insert(index_e, 'е')
                    Emps().put_in_field(lis_name)
                    Bs().wait_until_find(Lem.fio_employees)
                    fio_s = Lem.fio_employees
                    count = 0
                    """Проверяем есть ли в имя в списке который получили по тому же имени с заменой Ё на Е"""
                    for j in range(len(fio_s)):
                        find_name = fio_s[j].text.split(' ')
                        if name == find_name[1]:
                            count += 1
                            Emps().clear_field()
                            break
                    assert count >= 1, f"Выбран фильтр по имени - {name} с буквой Е вместо Ё," \
                                       f" но поиск не находит данного сотрудника или сотрудников с именем - {name}"
            """Проверяем побуквенно есть ли в фамилии буква 'Ё', если есть то меняем её на 'Е' и вставляем в поиск"""
            for d in range(len(last_name)):
                if last_name[d].lower() == 'ё':
                    lis_name = list(last_name.lower())
                    index_e = lis_name.index('ё')
                    lis_name.remove('ё')
                    lis_name.insert(index_e, 'е')
                    Emps().put_in_field(lis_name)
                    Bs().wait_until_find(Lem.fio_employees)
                    fio_s = Lem.fio_employees
                    count = 0
                    """Проверяем есть ли в фамилия в списке который получили по той же фамилии но с заменой Ё на Е"""
                    for f in range(len(fio_s)):
                        find_name = fio_s[f].text.split(' ')
                        if last_name == find_name[0]:
                            count += 1
                            Emps().clear_field()
                    assert count >= 1, f"Выбран фильтр по фамилии- {last_name} с буквой Е вместо Ё, но поиск не " \
                                       f"находит данного сотрудника или сотрудников с фамилией - {last_name}"
