from selene.api import *


class LocatorsEmp:
    title_employees = s(by.xpath('//h1'))
    cities_list = ss(by.xpath('//div[1]/select/option'))
    department_list = ss(by.xpath('//div[2]/select/option'))
    fio_employees = ss(by.xpath('//div[2]/div/div/div/div[1]/a'))
    ava_employees = ss(by.xpath('//div[2]/div/div/a'))
    find_field = s(by.xpath('//main/div/div/div[1]/div[1]/input'))
    department_user = ss(by.xpath('//div[2]/div/div/div/div[2]/div'))
    number_employees = s(by.xpath('//p'))
    close_find_field = s(by.xpath('//div/div/div[1]/div[1]/button'))
