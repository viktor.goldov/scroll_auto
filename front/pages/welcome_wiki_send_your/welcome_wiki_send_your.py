from selene.api import *


class LocatorsWelcomeWikiSend:
    title_welcome_wiki = s(by.xpath('/html/body/div/div[2]/div[1]/div/div/div[1]/h1'))
    title_booking = s(by.xpath('//h1'))
    button_cancel_modal_window = s(by.xpath('//div/div/div/div/button'))
    button_go_modal_window = s(by.xpath('//a/div'))
