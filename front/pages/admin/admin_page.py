from selene.api import *


class LocatorsAdmin:
    admin_title = s(by.class_name('i-settings__title'))
    admin_search_field = s(by.xpath('//div[2]/div[1]/input'))
    tags_delete_buttons = ss(by.class_name('i-tags__tagList-button'))
    add_tag_button = s(by.class_name('i-button'))
    tag_list = ss(by.class_name('i-tags__tagList-text'))
