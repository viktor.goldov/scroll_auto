import allure
from front.src.helpers.BaseSteps import BaseSteps as Bs
from front.pages.admin.admin_page import LocatorsAdmin as La


class AdminChecks:
    @allure.step("Проверить создался ли тэг")
    def check_create_tag(self, letters):
        count = 0
        Bs().wait_until_find(La.tag_list)
        for i in range(len(La.tag_list)):
            if '#' + letters == La.tag_list[i].text:
                count += 1
                break
        assert count > 0, f'Тэг {letters} не добавлен'

    @allure.step("Проверить не создался ли тэг")
    def check_not_create_tag(self, letters):
        count = 0
        Bs().wait_until_find(La.tag_list)
        for i in range(len(La.tag_list)):
            if '#' + letters == La.tag_list[i].text:
                count += 1
                break
        assert count == 0, f'Тэг {letters} добавлен, но не должен был'

    @allure.step("Проверить добавляется ли тэг с невалидным символом")
    def check_invalid_symbols_insert(self):
        letters = list('=!/(),;:!"-_')
        for i in range(len(letters)):
            La.admin_search_field.send_keys(letters[i])
            La.add_tag_button.click()
            AdminChecks().check_not_create_tag(letters[i])
            La.admin_search_field.clear()

    @allure.step("Проверить создался ли тэг на более 68 символов")
    def check_create_tag_up_68(self, letters):
        count = 0
        count2 = 0
        Bs().wait_until_find(La.tag_list)
        for i in range(len(La.tag_list)):
            list_tag_letters = list(La.tag_list[i].text)
            for j in range(len(list_tag_letters)):
                list_letters = list('#' + letters)
                if list_letters[j] == list_tag_letters[j]:
                    count += 1
                    if count == len(list_tag_letters):
                        count2 = len(list_tag_letters)
                        break
            if count == count2:
                break
            else:
                count = 0
        assert count == count2, f'Тэг {letters} не добавлен'
        assert count > 0, f'Тэг {letters} не добавлен'

    @allure.step("Проверить НЕ создался ли тэг на более 68 символов")
    def check_not_create_tag_up_68(self, letters):
        count = 0
        count2 = 0
        Bs().wait_until_find(La.tag_list)
        for i in range(len(La.tag_list)):
            list_tag_letters = list(La.tag_list[i].text)
            for j in range(len(list_tag_letters)):
                list_letters = list('#' + letters)
                if list_letters[j] == list_tag_letters[j]:
                    count += 1
                    if count == len(list_tag_letters):
                        count2 = len(list_tag_letters)
                        break
            if count == count2:
                break
            else:
                count = 0
        assert count == count2, f'Тэг {letters} добавлен'
        assert count == 0, f'Тэг {letters} добавлен'
