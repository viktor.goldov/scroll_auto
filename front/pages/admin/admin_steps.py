from front.pages.admin.admin_page import LocatorsAdmin as La
from front.src.helpers.BaseSteps import BaseSteps as Bs
import string as s
import random as r
import allure


class AdminSteps:
    @allure.step("Сформировать строку содержащую кириллицу в верхнем и нижнем регистре + цифры")
    def string_high_low_num_rus(self):
        alf_low = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
        alf_high = 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'
        my_string = ''.join(r.choices(alf_low+alf_high+s.digits, k=30))
        return my_string

    @allure.step("Сформировать строку содержащую латиницу в верхнем и нижнем регистре + цифры")
    def string_high_low_num_en(self):
        my_string = ''.join(r.choices(s.ascii_letters+s.digits, k=30))
        return my_string

    @allure.step("Сформировать строку содержащую буквы в верхнем и нижнем регистре + цифры в нужном количестве")
    def string_high_low_num_many(self, count):
        alf_low = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
        alf_high = 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'
        my_string = ''.join(r.choices(alf_low+alf_high+s.ascii_letters+s.digits, k=count))
        return my_string

    @allure.step("Удалить тэг")
    def delete_tag(self, letters):
        Bs().wait_until_find(La.tag_list)
        for i in range(len(La.tag_list)):
            if '#' + letters == La.tag_list[i].text:
                La.tags_delete_buttons[i].click()
