from src.data.endpoints import Endpoints
from src.data.data import DataForRicept as Dr
from src.data.data import DataForTag as Dt
from src.data.data import DataForIng as Di
from src.data.data import DataForDrink as Dd
from src.helpers.base_steps import BaseSteps
from src.data.data_sql import DataSQL
import allure


class UseElementsForCheck:
    code_200 = 200
    code_300 = 300
    code_400 = 400
    code_500 = 500


class Checks:
    @allure.step("Проверить get эндпоинта cocktails")
    def check_get_cock(self):
        r = BaseSteps().get(Endpoints.cocktails)
        code = r.status_code
        assert code == UseElementsForCheck.code_200, "\033[3m\033[36m\033{}".format(f'\nНе работает не хрена,'
                                                                                    f' код ответа:"{code}"')
        print("\033[5m\033[32m\033{}".format(f'\nКод ответа:"{code}"'))

    @allure.step("Проверить создался ли рецепт")
    def check_new_ricept(self):
        get_id = BaseSteps().get_id(endpoint=Endpoints.ricepts, body=Dr)
        assert get_id is not None, print("\033[5m\033[31m\033{}".format(f'\nРецепта с описанием "{Dr["description"]}"'
                                                                        f' \nНе найдено!!!'))
        print(f'\nРецепт найден c id: "{get_id}"')

    @allure.step("Проверить создался ли тэг")
    def check_new_tag(self):
        get_id = BaseSteps().get_id(endpoint=Endpoints.tags, body=Dt)
        assert get_id is not None, print("\033[5m\033[31m\033{}".format(f'\nТэга с названием "{Dt["name"]}" '
                                                                        f'\nНе найдено!!!'))
        print(f'\nТег найден c id: "{get_id}"')

    @allure.step("Проверить создался ли ингредиент")
    def check_new_ing(self):
        get_id = BaseSteps().get_id_by_name(endpoint=Endpoints.ingredients, body=Di)
        assert get_id is not None, print("\033[5m\033[31m\033{}".format(f'\nИнгредиента с названием "{Di["name"]}" '
                                                                        f'\nНе найдено!!!'))
        print(f'\nИнгридиент  найден с  id: "{get_id}"')

    @allure.step("Проверить создался ли коктейль")
    def check_new_drink(self):
        get_id = BaseSteps().get_id_by_name(endpoint=Endpoints.cocktails, body=Dd)
        assert get_id is not None, print("\033[5m\033[31m\033{}".format(f'\n Коктейля "{Dd["name"]}" не найдено!!!'))
        print(f'\nВаш коктейль создан c id: "{get_id}"')

    @allure.step("Проверить создался ли рецепт в базе данных")
    def check_db_new_ricept(self):
        request_db = BaseSteps().sent_to_db(sql=DataSQL().sql_select_ricept())
        assert request_db.rowcount != 0, f'Рецепта с описанием "{Dr["description"]}" в БД не найдено'
        col_name = [desc[0] for desc in request_db.description]
        for row in request_db:
            zipped_object = zip(col_name, row)
            print("\033[5m\033[32m\033{}".format(f'\n Рецепт создан:\n "{list(zipped_object)}"'))
        request_db.close()

    @allure.step("Проверить создался ли тэг в базе данных")
    def check_db_new_tag(self):
        request_db = BaseSteps().sent_to_db(sql=DataSQL().sql_select_tag())
        assert request_db.rowcount != 0, f'Тэга с названием "{Dt["name"]}" в БД не найдено'
        col_name = [desc[0] for desc in request_db.description]
        for row in request_db:
            zipped_object = zip(col_name, row)
            print("\033[5m\033[32m\033{}".format(f'\n Тег создан:\n "{list(zipped_object)}"'))
        request_db.close()

    @allure.step("Проверить создался ли ингредиент в базе данных")
    def check_db_new_ing(self):
        request_db = BaseSteps().sent_to_db(sql=DataSQL().sql_select_ing())
        assert request_db.rowcount != 0, f'Ингредиента с названием "{Di["name"]}" в БД не найдено'
        col_name = [desc[0] for desc in request_db.description]
        for row in request_db:
            zipped_object = zip(col_name, row)
            print("\033[5m\033[32m\033{}".format(f'\n Ингридиент  создан:\n "{list(zipped_object)}"'))
        request_db.close()

    @allure.step("Проверить создалась ли композиция в базе данных")
    def check_db_new_comp(self):
        request_db = BaseSteps().sent_to_db(sql=DataSQL().sql_select_comp())
        assert request_db.rowcount != 0, f'Композиции с id "{Di["compositionDtoList"]}" в БД не найдено'
        col_name = [desc[0] for desc in request_db.description]
        for row in request_db:
            zipped_object = zip(col_name, row)
            print("\033[5m\033[32m\033{}".format(f'\n Композиция создана:\n "{list(zipped_object)}"'))
        request_db.close()

    @allure.step("Проверить создался ли коктейль в базе данных")
    def check_db_new_drink(self):
        request_db = BaseSteps().sent_to_db(sql=DataSQL().sql_select_drink())
        assert request_db.rowcount != 0, f'Коктейля с названием "{Dd["name"]}" в БД не найдено'
        col_name = [desc[0] for desc in request_db.description]
        for row in request_db:
            zipped_object = zip(col_name, row)
            print("\033[5m\033[32m\033{}".format(f'\n Коктейль создан:\n "{list(zipped_object)}"'))
        request_db.close()

    @allure.step("Проверить удалился ли рецепт")
    def check_del_ricept(self):
        get_id = BaseSteps().get_id(endpoint=Endpoints.ricepts, body=Dr)
        assert get_id is None, print("\033[5m\033[31m\033{}".format(f'\nРецепт не УДАЛЕН!!! его id: "{get_id}"'))
        print(f'\nВаш Рецепт УДАЛЕН!!!')

    @allure.step("Проверить удалился ли тэг")
    def check_del_tag(self):
        get_id = BaseSteps().get_id(endpoint=Endpoints.tags, body=Dt)
        assert get_id is None, print("\033[5m\033[31m\033{}".format(f'\nТэг не УДАЛЕН!!! его id: "{get_id}"'))
        print(f'\nВаш Тэг УДАЛЕН!!!')

    @allure.step("Проверить удалился ли ингредиент")
    def check_del_ing(self):
        get_id = BaseSteps().get_id_by_name(endpoint=Endpoints.ingredients, body=Di)
        assert get_id is None, print("\033[5m\033[31m\033{}".format(f'\nИнгредиент не УДАЛЕН!!! его id: "{get_id}"'))
        print(f'\nВаш Ингредиент  УДАЛЕН!!!')

    @allure.step("Проверить удалился ли коктейль")
    def check_del_drink(self):
        get_id = BaseSteps().get_id_by_name(endpoint=Endpoints.cocktails, body=Dd)
        assert get_id is None, print("\033[5m\033[31m\033{}".format(f'\nВаш коктейль НЕ УДАЛЕН!!!'
                                                                    f' вот его id: "{get_id}"'))
        print("\033[5m\033[32m\033{}".format(f'\nВаш коктейль  УДАЛЕН!!!'))

    @allure.step("Проверить удалился ли рецепт из базы данных")
    def check_db_del_ricept(self):
        request_db = BaseSteps().sent_to_db(sql=DataSQL().sql_select_del_ricept())
        col_name = [desc[0] for desc in request_db.description]
        for row in request_db:
            zipped_object = zip(col_name, row)
            assert request_db.rowcount == 0, f'\nРецепт не УДАЛЕН!!! вот его данные:\n "{list(zipped_object)}"'
        print("\033[5m\033[32m\033{}".format(f'\nВашего рецепта нет в БД он УДАЛЕН!!!'))
        request_db.close()

    @allure.step("Проверить удалился ли тэг из базы данных")
    def check_db_del_tag(self):
        request_db = BaseSteps().sent_to_db(sql=DataSQL().sql_select_tag())
        col_name = [desc[0] for desc in request_db.description]
        for row in request_db:
            zipped_object = zip(col_name, row)
            assert request_db.rowcount == 0, f'\nТэг не УДАЛЕН!!! вот его данные:\n "{list(zipped_object)}"'
        print("\033[5m\033[32m\033{}".format(f'\nВашего тэга нет в БД он УДАЛЕН!!!'))
        request_db.close()

    @allure.step("Проверить удалился ли ингредиент из базы данных")
    def check_db_del_ing(self):
        request_db = BaseSteps().sent_to_db(sql=DataSQL().sql_select_ing())
        col_name = [desc[0] for desc in request_db.description]
        for row in request_db:
            zipped_object = zip(col_name, row)
            assert request_db.rowcount == 0, f'\nИнгредиент не УДАЛЕН!!! вот его данные:\n "{list(zipped_object)}"'
        print("\033[5m\033[32m\033{}".format(f'\nВашего ингредиента нет в БД он УДАЛЕН!!!'))
        request_db.close()

    @allure.step("Проверить удалилась ли композиция из базы данных")
    def check_db_del_comp(self):
        request_db = BaseSteps().sent_to_db(sql=DataSQL().sql_select_comp())
        col_name = [desc[0] for desc in request_db.description]
        for row in request_db:
            zipped_object = zip(col_name, row)
            assert request_db.rowcount == 0, f'\nКомпозиция не УДАЛЕНА!!! вот её данные:\n "{list(zipped_object)}"'
        print("\033[5m\033[32m\033{}".format(f'\nВашей композиции нет в БД она УДАЛЕНА!!!'))
        request_db.close()

    @allure.step("Проверить удалился ли коктейль из базы данных")
    def check_db_del_drink(self):
        request_db = BaseSteps().sent_to_db(sql=DataSQL().sql_select_drink())
        col_name = [desc[0] for desc in request_db.description]
        for row in request_db:
            zipped_object = zip(col_name, row)
            assert request_db.rowcount == 0, f'\n Ваш коктейль НЕ УДАЛЕН!!! вот его данные:\n "{list(zipped_object)}"'
        print("\033[5m\033[32m\033{}".format(f'\nВашего коктейля нет в БД он УДАЛЕН!!!'))
        request_db.close()
