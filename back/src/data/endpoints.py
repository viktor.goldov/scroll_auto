from back.src.data.urls import Urlbase


class Endpoints:
    cocktails = f'{Urlbase.base_url}cocktails'
    compositions = f'{Urlbase.base_url}compositions'
    ingredients = f'{Urlbase.base_url}ingredients'
    ricepts = f'{Urlbase.base_url}ricepts'
    tags = f'{Urlbase.base_url}tags'
    login = f'{Urlbase.auf_url}auth/login'
