import psycopg2
import requests
import allure
from back.src.data.endpoints import Endpoints
from login_db import dbname, user, password_db, host


class BaseSteps:
    @allure.step("метод Get")
    def get(self, endpoint):
        r = requests.get(endpoint)
        return r

    @allure.step("метод Post")
    def post(self, endpoint, headers, body):
        r = requests.post(url=endpoint, headers=headers, json=body)
        return r

    @allure.step("метод Patch")
    def patch(self, endpoint, headers, body):
        r = requests.patch(url=endpoint, headers=headers, json=body)
        return r

    @allure.step("метод Put")
    def put(self, endpoint, headers, body):
        r = requests.put(url=endpoint, headers=headers, json=body)
        return r

    @allure.step("Авторизация")
    def get_token(self, email, password):
        payload = {'email': email, 'password': password}
        ra = requests.post(Endpoints.login, json=payload)
        au = ra.headers['Authorization']
        return au

    @allure.step("Отправить запрос в БД")
    def sent_to_db(self, sql):
        conn = psycopg2.connect(dbname=dbname, user=user,
                                password=password_db, host=host)
        conn.set_client_encoding('UTF8')
        cursor = conn.cursor()
        cursor.execute(sql)
        conn.commit()
        return cursor

    @allure.step("метод Delete")
    def delete(self, endpoint, headers):
        r = requests.delete(url=endpoint, headers=headers)
        return r

    @allure.step("Взять id")
    def get_id(self, endpoint, body):
        rg = BaseSteps().get(endpoint)
        body_rg = rg.json()
        for i in range(len(body_rg)):
            if body.items() <= body_rg[i].items():
                return body_rg[i]['id']

    @allure.step("Взять id по name")
    def get_id_by_name(self, endpoint, body):
        rg = BaseSteps().get(endpoint)
        body_rg = rg.json()
        for i in range(len(body_rg)):
            if body['name'] == body_rg[i]['name']:
                return body_rg[i]['id']
