import requests
from login import email, password
from src.helpers.base_steps import BaseSteps
from src.data.endpoints import Endpoints
from src.data.endpoints_id import EndpointsId
from src.data.data import DataForRicept as Dr
from src.data.data import DataForIng as Di
from src.data.data import DataForTag as Dt
from src.data.data import DataForDrink as Dd
from src.data.data_sql import DataSQL
import allure


class Steps:
    @allure.step("Создать рецепт")
    def create_ricept(self):
        au = BaseSteps().get_token(email, password)
        body_rp = (dict(description=Dr["description"], orderRicept=Dr["orderRicept"]))
        rp = BaseSteps().post(Endpoints.ricepts, {'Authorization': au}, body_rp)
        assert rp.status_code == requests.codes.ok

    @allure.step("Создать тэг")
    def create_tags(self):
        au = BaseSteps().get_token(email, password)
        body_rp = (dict(name=Dt['name']))
        rp = BaseSteps().post(Endpoints.tags, {'Authorization': au}, body_rp)
        assert rp.status_code == requests.codes.ok

    @allure.step("Создать ингредиент")
    def create_ing(self):
        au = BaseSteps().get_token(email, password)
        body_rp = (dict(compositionList=Di['compositionDtoList'], name=Di['name']))
        rp = BaseSteps().post(Endpoints.ingredients, {'Authorization': au}, body_rp)
        assert rp.status_code == requests.codes.ok

    @allure.step("Создать композицию через БД (без связи)")
    def create_compositions_sql(self):
        request_db = BaseSteps().sent_to_db(sql=DataSQL().sql_create_compositions(unit='кг', value=3))
        request_db.close()

    @allure.step("Обновить композицию через БД (связь с ингредиентом)")
    def update_compositions_sql(self):
        request_db = BaseSteps().sent_to_db(sql=DataSQL().sql_update_compositions())
        request_db.close()

    @allure.step("Создать коктейль")
    def create_drink(self):
        au = BaseSteps().get_token(email, password)
        rp = BaseSteps().post(Endpoints.cocktails, {'Authorization': au}, Dd)
        assert rp.status_code == requests.codes.ok

    @allure.step("Удалить рецепт")
    def delete_ricept(self):
        au = BaseSteps().get_token(email, password)
        rp = BaseSteps().delete(EndpointsId().ricepts(), {'Authorization': au})
        assert rp.status_code == requests.codes.ok

    @allure.step("Удалить тэг")
    def delete_tags(self):
        au = BaseSteps().get_token(email, password)
        rp = BaseSteps().delete(EndpointsId().tags(), {'Authorization': au})
        assert rp.status_code == requests.codes.ok

    @allure.step("Удалить ингредиент")
    def delete_ing(self):
        au = BaseSteps().get_token(email, password)
        rp = BaseSteps().delete(EndpointsId().ingredients(), {'Authorization': au})
        assert rp.status_code == requests.codes.ok

    @allure.step("Удалить коктейль")
    def delete_drink(self):
        au = BaseSteps().get_token(email, password)
        rp = BaseSteps().delete(EndpointsId().cocktails(), {'Authorization': au})
        assert rp.status_code != requests.codes.ok
